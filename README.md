# Yang's Code PlayGround
This is a play ground with collection of past prototype project code using deeplearning especially with Tesorflow/Keras frameworks.

# List of key work built within the collection: 
*  MotionCorrect Image Augmentator:
    * Used to generate marker on top of objects of randomly cropped background to facilitate deep learning training. 
*  MotionCorrect Marker Presence Detection CNN
*  MotionCorrect Marker Orientation Inference Regression CNN: 
    * Also see See https://github.com/dyt811/PoseTracker for the submitted version and documentation. 
    * https://www.microsoft.com/en-us/ai/ai-lab-stories?activetab=pivot1:primaryr8
*  Weather Data Challenge: 
    *  See more detailed submission documentation at Google Doc [here](https://docs.google.com/document/d/1kjPmaxSYFCnwZBuzKBO2HdkxX22KmMzplqenLAZoPrE/edit?usp=sharing). 
*  Kaggle Dog DCGAN: 
    * Forked network architecture that is being integrated for Kaggle competition. 
    * Submodule: https://github.com/dyt811/Kaggle_DCGAN_Dogs is part of this main repository, which is the frame work. 
    * Currently improving soft labeling process.

# Todo:

* Variational Autoencoders
* Transfer Learning Example
* MLFlow accomodations. 