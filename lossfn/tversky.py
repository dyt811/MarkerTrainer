# Generalized DICE
import tensorflow as tf


class tversky_loss:
    def __init__(self, beta):
        self.beta = beta

    # Source:
    def loss(self, y_true, y_pred):
        numerator = tf.reduce_sum(y_true * y_pred, axis=-1)
        denominator = (
            y_true * y_pred
            + self.beta * (1 - y_true) * y_pred
            + (1 - self.beta) * y_true * (1 - y_pred)
        )

        return 1 - (numerator + 1) / (tf.reduce_sum(denominator, axis=-1) + 1)
