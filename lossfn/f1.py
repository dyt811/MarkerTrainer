import keras.backend as K
import tensorflow as tf


def f1_metric(y_true, y_pred):
    """
    Calculate a balanced measure of recall and precision.
    Source: https://github.com/keras-team/keras/issues/10018
    Inspired by original stack overflow at here: https://stackoverflow.com/a/45305384/5210098
    :param y_true:
    :param y_pred:
    :return: a SINGLE number representing the overall measurement (NOT A TENSOR)
    """

    def recall(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


def f1_metric_elementwise(y_true, y_pred):
    """
    This compute the elementwise F1 score at per pixel/voxel level. Notice this COLLAPSE the last  dimension (e.g. across color or GROUND TRUTH).
    Adopted from source: https://lars76.github.io/neural-networks/object-detection/losses-for-segmentation/
    :param y_true:
    :param y_pred:
    :return: tensor matching the y_true in dimension.
    """

    numerator = 2 * tf.reduce_sum(y_true * y_pred, axis=-1)
    denominator = tf.reduce_sum(y_true + y_pred, axis=-1)

    return 1 - (numerator + 1) / (denominator + 1)


def f1_metric3(y_true, y_pred):
    numerator = 2 * tf.reduce_sum(y_true * y_pred)
    denominator = tf.reduce_sum(y_true + y_pred)

    return 1 - numerator / denominator
