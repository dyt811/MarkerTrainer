import tensorflow as tf
from .loss_uti import convert_to_logits

# Note must instantiate the class with a particular beta before using its loss.
# e.g.
# wce = weighted_cross_entropy(0.5)
# wce.loss(y_true, y_pred)


class weighted_cross_entropy:
    """
    Weighted Cross Entropy Loss Implementation sourced from # Source: https://lars76.github.io/neural-networks/object-detection/losses-for-segmentation/
    :param beta:
    To decrease the number of false negatives, set Beta > 1,
    To decrease the number of false positives, set Beta < 1
    :return:
    """

    def __init__(self, beta):
        self.beta = beta

    def loss(self, y_true, y_pred):
        """
        Compute The Loss
        :param y_true:
        :param y_pred:
        :return:
        """
        # Conver tot logits before calling TF
        y_pred = convert_to_logits(y_pred)

        # Call the native TF implementation of the weighted cross entropy with logit
        loss = tf.nn.weighted_cross_entropy_with_logits(
            logits=y_pred, targets=y_true, pos_weight=self.beta
        )

        # or reduce_sum and/or axis=-1
        return tf.reduce_mean(loss)
