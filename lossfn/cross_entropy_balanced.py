# source: https://lars76.github.io/neural-networks/object-detection/losses-for-segmentation/
from .loss_uti import convert_to_logits
import tensorflow as tf


class balanced_cross_entropy:
    def __init__(self, beta):
        self.beta = beta

    def loss(self, y_true, y_pred):
        y_pred = convert_to_logits(y_pred)
        pos_weight = self.beta / (1 - self.beta)
        loss = tf.nn.weighted_cross_entropy_with_logits(
            logits=y_pred, targets=y_true, pos_weight=pos_weight
        )

        # or reduce_sum and/or axis=-1
        return tf.reduce_mean(loss * (1 - self.beta))
