import os
import tensorflow as tf
import cv2
from typing import Callable
import logging
import numpy as np

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def convert_to_logits(y_pred):
    # see https://github.com/tensorflow/tensorflow/blob/r1.10/tensorflow/python/keras/backend.py#L3525
    y_pred = tf.clip_by_value(
        y_pred, tf.keras.backend.epsilon(), 1 - tf.keras.backend.epsilon()
    )

    return tf.log(y_pred / (1 - y_pred))


def calculate_mean_diff(img1, img2):
    """
    Calculate the difference between the mean of two images
    :param img1: The fiirst images
    :param img2: The second images
    :returns Returns the positive aboslute difference between the mean values
    """
    img1_mean = tf.reduce_mean(img1)
    img2_mean = tf.reduce_mean(img2)

    return tf.abs(img1_mean - img2_mean)


def calculate_min_diff(img1, img2):
    """
    Calculate the difference between the min of two images
    :param img1: The fiirst images
    :param img2: The second images
    :returns Returns the difference between the min values
    """
    img1_min = tf.reduce_min(img1)
    img2_min = tf.reduce_min(img2)

    return tf.abs(img1_min - img2_min)


def calculate_max_diff(img1, img2):
    """
    Calculate the difference between the max of two images
    @param {y_true|y_predict} : The two images
    @returns {tf.abs(img1_max - img2_max)} Returns the difference between the max values
    """
    img1_max = tf.reduce_max(img1)
    img2_max = tf.reduce_max(img2)

    return tf.abs(img1_max - img2_max)


def check_loss_function(path_image_true, path_image_predicted, loss_func: Callable):
    """
    Load the images after validating their paths
    :param path_image_true:
    :param path_image_predicted:
    :return:
    """
    if not os.path.exists(path_image_true):
        logger.critical("Input image path does not exist")
        return
    elif not os.path.exists(path_image_predicted):
        logger.critical("Predicted image path does not exist")
        return

    image_true = cv2.imread(path_image_true)
    image_predicted = cv2.imread(path_image_predicted)
    assert image_true.shape == image_predicted.shape

    return call_lossfunc(image_true, image_predicted, loss_func)


def call_lossfunc(img1, img2, loss_func: Callable):
    """
    Calculate the score between two images using tensorflow session run
    :param {y_true|y_predict|maxValue} : The two tensor images and the max values of a pixel in the two images (default value is 1.0 for normalized images)
    :returns {scoreValue} Returns a numpy array of the score value of each images
    """

    # The shape of these two MUSt be identical.
    assert img1.shape == img2.shape
    height = img2.shape[0]
    width = img2.shape[1]
    depth = img2.shape[2]

    logger.info(f"Input Size: {height}x{width}x{depth}")

    img1_placeholder = tf.placeholder(
        tf.float32, shape=(height, width, depth), name="img1_placeholder"
    )
    img2_placeholder = tf.placeholder(
        tf.float32, shape=(height, width, depth), name="img2_placeholder"
    )

    with tf.Session() as sess:
        score = loss_func(img1_placeholder, img2_placeholder)
        scoreValue = score.eval(
            feed_dict={img1_placeholder: img1, img2_placeholder: img2}
        )
    return scoreValue


if __name__ == "__main__":
    from lossfn.f1 import f1_metric, f1_metric_elementwise, f1_metric3

    result = check_loss_function(
        r"C:\Git\MarkerTrainer\data_mnist\holdout_train\00000.jpeg",
        r"C:\Git\MarkerTrainer\data_mnist\holdout_train\00001.jpeg",
        f1_metric_elementwise,
    )
    print(result)
