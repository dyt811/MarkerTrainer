#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: scoreFunction.py
Author: Kevin Gauthier, open sourced in WeatherData challlenge. Adapted by Yang Ding
Creation date: November 11th 2018, updated in 2019-09-19T002350EST
Description: Script that calculate the score of the tests images. (function getScore)
Required package: numpy and tensorflow
"""

import tensorflow as tf

from lossfn.loss_uti import calculate_mean_diff, calculate_min_diff, calculate_max_diff


def loss_mse_diff_SSIM_composite(y_true, y_predict, maxValue=1):
    """
    Calculate the custome SSIM composite score between two images.
        It is composed of SSIM, MSE, Mean/Min/Max differences

    :param: The two tensor images and the max values of a pixel in the two images
        NOTE: they should be gray scale images.
    :returns Returns the difference between the mean values
    """

    # Calculation of all the intermediate score
    ssim = tf.image.ssim(y_true, y_predict, max_val=maxValue)
    mse = tf.reduce_sum(tf.math.squared_difference(y_true, y_predict))

    mean_difference = calculate_mean_diff(y_true, y_predict)
    min_difference = calculate_min_diff(y_true, y_predict)
    max_difference = calculate_max_diff(y_true, y_predict)

    # Score function
    score = (2 - 2 * ssim) + mse + mean_difference + min_difference + max_difference

    return score


def loss_mae_diff_SSIM_composite(y_true, y_predict, maxValue=1):
    """
    Calculate the custome SSIM composite score between two images.
        It is composed of SSIM, MAE, Mean/Min/Max differences

    :param: The two tensor images and the max values of a pixel in the two images
        NOTE: they should be gray scale images.
    :returns Returns the difference between the mean values
    """

    # Transpose the tensors to have format (NHWC) / numpy array format given is (NCHW)
    # img1_transpose = tf.transpose(y_true, [0, 2, 3, 1])
    # img2_transpose = tf.transpose(y_predict, [0, 2, 3, 1])

    # Calculation of all the intermediate score
    ssim = tf.image.ssim(y_true, y_predict, max_val=maxValue)
    mae = tf.reduce_sum(tf.abs(tf.subtract(y_true, y_predict))) / 320 / 240
    mean_difference = calculate_mean_diff(y_true, y_predict)
    min_difference = calculate_min_diff(y_true, y_predict)
    max_difference = calculate_max_diff(y_true, y_predict)

    # Score function
    score = (2 - 2 * ssim) + mae + mean_difference + min_difference + max_difference

    return score


def loss_SSIM(y_true, y_predict, maxValue=1.0):
    """
    Calculate the SSIM_loss score between two images.
    :param: The two tensor images and the max values of a pixel in the two images
    :returns Returns SSIM_loss, which can only be between 0 (identical image) and 2
    """

    # Calculation of all the intermediate score
    ssim = tf.image.ssim(y_true, y_predict, max_val=maxValue)

    # Score function
    score = (
        1 - ssim
    )  # this change the SSIM range from 0 to 2 and allow loss minimization

    return score
