## /aug

This folder contains most functions with regard to data augmentation, usually used in image augmentation mostly via ImgAug. 

### background
* functions related to background generation for image superimposition

### foreground
* functions related to foreground object manipulation. 
* e.g. our optical markers from MotionCorrect

### merge
* used to generate merged images