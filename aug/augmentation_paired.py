import sys
from pathlib import Path

print(sys.path)
from PythonUtils.PUFile import unique_name, filelist_delete
from PythonUtils.PUFolder import recursive_list, create
from PythonUtils.PUJson import write_json
import shutil
from aug.foreground.load_batch import load_from_filelist
from tqdm import trange
import copy
import logging

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)
from aug.augmentation_sequence import TrailMixAug
from aug.augmentation import save_list_images


def dual_augment_folder(
    path_images: Path,
    path_groundtruth_images: Path,
    aug_seg,
    iterations,
    path_out: Path,
):
    """
    Duplicate the entire folder X iterations before augmenting the entire folder, while keeping the gound truth segmentation in tact and generate identical file name but in two different folders. Useful primarily in the situation of image segmentation aug while preserving ground truth pairing.

    It does a batch aug process per 1000 images because memory cannot load that many more at the same time.

    :param path_images:
    :param path_out:
    :param aug_seg:
    :param iterations:
    :return:
    """

    # Input must have matching number of ground truth.
    list_input = recursive_list(path_images.absolute())
    list_labels = recursive_list(path_groundtruth_images.absolute())
    assert len(list_input) == len(list_labels)

    # Duplicate the folder X times.
    list_input_files, list_label_files = dual_duplicate(
        list_input, path_out, iterations, list_labels
    )

    logger.info("Augmenting files from folder:" + str(path_images))

    # Decide if to do batch aug or all aug.
    with trange(len(list_input_files)) as pbar:

        while len(list_input_files) != 0:
            # While the files to be processed < 1000, just read the damn thing.
            if len(list_input_files) < 1000:
                list_to_process = copy.deepcopy(
                    list_input_files
                )  # Assign all remaining items to processing data.
                list_input_files.clear()  # Empty the list to trigger exit condition.

            # When there are more files, we actually try to process 1k images at a time, augment, write out. then.
            else:
                # Transfer the top of the list to another variable.
                list_to_process = list_input_files[0:999]

                # Truncate original list.
                del list_input_files[0:999]

            # COMMON PORTION. Previous section only modify the input_augment_files list and what needs to be processed.
            # Load images into a giant matrices from the TEMP folder
            images_ndarray = load_from_filelist(list_to_process)

            # Now that all images are in memory, time to delete all these "source" input files.
            filelist_delete(list_to_process)

            # Augment the giant matrices
            images_augmented = aug_seg.augment_images(images_ndarray)

            # logger.info("Saving augmenting images to: " + str(path_out))

            # Save the augmented images out to the list of paths
            save_list_images(
                images_augmented, path_out.absolute() / "train", list_to_process
            )

            # Update progress bar of the aug
            pbar.update(len(list_input_files))
        pbar.close()
    logger.info("All images augmented.")

    # Keep a record of the augmentation details
    augmentation_details = {}
    augmentation_details["path_train"] = str(path_images)
    augmentation_details["path_label"] = str(path_groundtruth_images)
    augmentation_details["path_out"] = str(path_out)
    augmentation_details["aug_sequence"] = str(aug_seg)
    path_json = path_out / (unique_name() + "_AugmentationDetails.json")
    write_json(path_json, augmentation_details)


def dual_duplicate(
    list_input: list, path_output: Path, iterations: int, list_labels: list
):
    """
    The function takes a list of files and duplicate them with unique names X times into the output folder, then return the updated input list.
    :param list_files:
    :param path_output: this is the folder which MUST contain TRAIN and LABEL which the images will be output into.
    :param iterations:
    :return:
    """

    # Create the respective folders to contain the output.
    assert len(list_input) == len(list_labels)
    path_train = path_output.joinpath("train")
    path_label = path_output.joinpath("label")
    create(path_train)
    create(path_label)

    logger.debug(f"Duplication files for {str(iterations)} iteraitons.")
    # Duplicate the folder x times
    for x in range(0, iterations):
        # each time, duplicate all the files within it
        for index, file in enumerate(list_input):

            # Make sure to assign UNIQUE name, but they reside in differnet folders.
            file_name = unique_name()

            # Copy train file
            file_train_path = path_train.joinpath(f"{file_name}.jpeg")
            shutil.copyfile(file, file_train_path)

            # Copy label file
            file_label_path = path_label.joinpath(f"{file_name}.jpeg")
            shutil.copyfile(list_labels[index], file_label_path)

    # Recursively list the output folder
    train_files = recursive_list(path_train)
    label_files = recursive_list(path_label)

    return train_files, label_files


# When launching main, the path of the python is set to execute from THIS particular directory and will have trouble recognizing higher level as packages.
if __name__ == "__main__":

    # Get the right aug sequence.
    aug_seq = TrailMixAug()
    dual_augment_folder(
        Path(r"../data_trailmix/train"),
        Path(r"../data_trailmix/label"),
        aug_seq,
        200,
        Path(fr"../data_trailmix/augmentation_{unique_name()}"),
    )
