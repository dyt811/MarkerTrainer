from aug.augmentation_sequence import Aug100px
from aug.augmentation import augment_to_folder, augment_image, augment_folder
from imageio import imread


def test_augment_to_folder():
    augment_to_folder(r"C:\git\MarkerTrainer\aug\foreground\Prime",
                      r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\augmented_marker",
                      Aug100px(),
                      20000,
                      "Aug100ImgAug"
                      ),


def test_augment_image():
    augment_image(r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\Cropped\cropped_images_2021-01-17T13_26_59.909728",
                  r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\augmented_bg\\",
                  Aug100px(),
                  2,
                  ),


def test_sanity():
    images_different_sizes = [
        imread("https://upload.wikimedia.org/wikipedia/commons/e/ed/BRACHYLAGUS_IDAHOENSIS.jpg"),
        imread("https://upload.wikimedia.org/wikipedia/commons/c/c9/Southern_swamp_rabbit_baby.jpg"),
        imread("https://upload.wikimedia.org/wikipedia/commons/9/9f/Lower_Keys_marsh_rabbit.jpg")
    ]
    aug_seq = Aug100px()
    aug_seq(images=images_different_sizes, )


def test_augment_folder():
    augment_folder(r"C:\git\TestDownloads\Cropped\cropped_images_2021-01-17T13_26_59.909728",
                  r"C:\git\TestDownloads\augmented_bg",
                  Aug100px(),
                  2,
                  )
