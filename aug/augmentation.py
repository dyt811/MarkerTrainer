import sys
from pathlib import Path

print(sys.path)
from PythonUtils.PUFile import unique_name, filelist_delete, duplicates_into_folders
from PythonUtils.PUFolder import recursive_list
import imageio
import os
import shutil
from aug.foreground.load_batch import load_from_filelist
from tqdm import tqdm, trange
import copy
import logging

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


def save_list_images(images_aug_collection, out_path, list_file_names):
    """
    Take an augmented image collection an a path, then iterate through the collection to save them.
    :param images_aug_collection: the data bundle that has been properly augmented
    :param out_path: the output ROOT path where all images will reside.
    :return:
    """
    assert len(images_aug_collection) == len(list_file_names)
    for index, image in enumerate(images_aug_collection):
        # import matplotlib.pyplot as plt
        # plt.imshow(image, aspect="auto")
        # plt.show()

        # Generate the time stamp required.
        filename = os.path.join(out_path, os.path.basename(list_file_names[index]))

        # logger.info("Saving" + filename)

        # Saving the file.
        imageio.imwrite(filename, image)


def save_images(images_aug_collection, out_path):
    """
    Take an augmented image collection an a path, then iterate through the collection to save them.
    :param images_aug_collection: the data bundle that has been properly augmented
    :param out_path: the output ROOT path where all images will reside.
    :return:
    """
    for image in tqdm(images_aug_collection):
        # import matplotlib.pyplot as plt
        # plt.imshow(image, aspect="auto")
        # plt.show()

        # Generate the time stamp required.
        filename = os.path.join(out_path, unique_name() + ".png")

        logger.info("Saving" + filename)

        # Saving the file.
        imageio.imwrite(filename, image)


def augment_image(image_path, out_path, aug_seq, iterations):
    """
    Augment ONE image over ITERATIONS and output to the path, with the aug sequence given.
    :param image_path:
    :param out_path:
    :param aug_seq:
    :param iterations:
    :return:
    """

    # Duplicate the images x times.
    for x in range(0, iterations):
        new_file_name = os.path.join(out_path, unique_name() + ".png")
        shutil.copyfile(image_path, new_file_name)

    augment_folder(out_path, out_path, aug_seq, 1)


def augment_to_folder(
    input_path, output_path, aug_sequence, iterations, aug_description
):
    """
    A wrapped version of FolderAugmentator that create a UNQIUE subfolder.
    :param input_path: input path that contain the list of files.
    :param output_path: output root path, a
    :param aug_sequence: the aug seuqence that will be applied.
    :param iterations: number of the time to augment these input folders.
    :param aug_description: additional text string to be part of the folder name.
    :return: the path of the created SUBFOLDER
    """

    # Set and create the path of the augmented background.
    augmentation_folder = os.path.join(output_path, unique_name() + aug_description)
    os.makedirs(augmentation_folder)

    # Augment from input folder into the output folder.
    augment_folder(input_path, augmentation_folder, aug_sequence, iterations)

    # return the path.
    return augmentation_folder


def augment_folder(input_folder_path, out_folder_path, aug_seg, iterations):
    """
    Duplicate the entire folder X iterations before augmenting the entire folder, using the aug sequence provided over
    the number of times requests.
    It does a batch aug process per 1000 images because memory cannot load that many more at the same time.
    :param input_folder_path:
    :param out_folder_path:
    :param aug_seg:
    :param iterations:
    :return:
    """

    input_files = recursive_list(input_folder_path)

    # Duplicate the folder X times.
    input_augment_files = duplicates_into_folders(input_files, out_folder_path, iterations)

    logger.info("Augmenting files from folder:" + out_folder_path)

    # Decide if to do batch aug or all aug.
    with trange(len(input_augment_files)) as pbar:

        while len(input_augment_files) != 0:
            # While the files to be processed < 1000, just read the damn thing.
            if len(input_augment_files) < 1000:
                processing_data = copy.deepcopy(
                    input_augment_files
                )  # Assign all remaining items to processing data.
                input_augment_files.clear()  # Empty the list to trigger exit condition.

            # When there are more files, we actually try to process 1k images at a time, augment, write out. then.
            else:
                # Transfer the top of the list to another variable.
                processing_data = input_augment_files[0:999]

                # Truncate original list.
                del input_augment_files[0:999]

            # COMMON PORTION. Previous section only modify the input_augment_files list and what needs to be processed.
            # Load images into a giant matrices from the TEMP folder
            images_ndarray = load_from_filelist(processing_data)

            # Augment the giant matrices
            images_augmented = aug_seg(images=images_ndarray)

            logger.info("Saving augmenting images to: " + out_folder_path)

            # Save the augmented images out to path.
            save_images(images_augmented, out_folder_path)

            # Now that all images are in memory, time to delete all these "source" input files.
            filelist_delete(processing_data)

            # Update progress bar of the aug
            pbar.update(len(input_augment_files))
        pbar.close()

    logger.info("All images augmented.")


# When launching main, the path of the python is set to execute from THIS particular directory and will have trouble recognizing higher level as packages.
if __name__ == "__main__":
    from aug.augmentation_sequence import MarkerAug

    # Get the right aug sequence.
    aug_seq = MarkerAug()

    # Load Prime images.
    # ImageAugmentator(r"E:\Gitlab\MarkerTrainer\overlay_data\Prime\100.png", r"E:\GitHub\MarkerTrainer\overlay_data\Altered", aug_seq, 10)

    augment_folder(
        r"E:\Gitlab\MarkerTrainer\foreground\Prime",
        r"C:\temp\AugMarker",
        aug_seq,
        10000,
    )
