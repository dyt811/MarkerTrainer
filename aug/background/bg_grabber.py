from google_images_download import google_images_download
from PythonUtils.folder import change
from pathlib import Path
import os

def downloadGoogleImages(arguments, inputfolder):
    """
    Using the GoogleDownloaderArgument to obtain a bunch of images.
    :param arguments:
    :return: a list of all the files downloaded
    """
    change(inputfolder)
    download_instance = google_images_download.googleimagesdownload()
    absolute_image_paths = download_instance.download(arguments)
    print(absolute_image_paths)   # printing absolute paths of the downloaded images

    return absolute_image_paths


def download_images(path_output: str or Path):
    """
    Download the entire list of images based on the parameters defined.
    :param config: the configuration for pathing.
    :return:
    """

    download_specs = {
        "keywords": "office,logos,black%20white,nature,work,home,class,black%20logo,white%20logo,design,abstract,futuristic,scene",
        "limit": 20,
        "chromedriver": r"C:\bin\chromedriver.exe",
        "size": ">6MP",
        "format": "jpg",
        "print_urls": True,
    }  # creating list of download_specs

    # download_specs = {"keywords":"Polar bears,baloons,Beaches","limit":20,"print_urls":True}

    bg_list = downloadGoogleImages(download_specs, path_output)
    return bg_list

