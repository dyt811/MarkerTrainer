# Source: https://stackoverflow.com/questions/54911436/how-to-implement-crelu-in-keras
import tensorflow as tf
import keras


class CRelu(tf.keras.layers.Layer):
    """
    A temporary wrapper of CRelu for tensorflow API
    """

    def __init__(self, axis=-1, **kwargs):
        self.axis = axis
        super(CRelu, self).__init__(**kwargs)

    def build(self, input_shape):
        super(CRelu, self).build(input_shape)

    def call(self, x):
        x = tf.nn.crelu(x, axis=self.axis)
        return x

    def compute_output_shape(self, input_shape):
        output_shape = list(input_shape)
        output_shape[-1] = output_shape[-1] * 2
        output_shape = tuple(output_shape)
        return output_shape

    def get_config(self, input_shape):
        config = {"axis": self.axis}
        base_config = super(CReLU, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


# Example usage.
def _conv_bn_crelu(x, n_filters, kernel_size):
    x = Conv2D(
        filters=n_filters, kernel_size=kernel_size, strides=(1, 1), padding="same"
    )(x)
    x = BatchNormalization(axis=-1)(x)
    x = CRelu()(x)
    return x
