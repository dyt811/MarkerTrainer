## /models
This is where the actual model weight and trained model checkpoints are saved. This folder is ignored by default to prevent .git folder become unnecessarily large. 
