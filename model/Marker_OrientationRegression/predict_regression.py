from keras.models import Sequential
from keras.preprocessing import image
from model.load import load_model
import os
import numpy as np
from tqdm import tqdm
from PythonUtils.PUFolder import recursive_list_re
from generator.csvgen import generate_csv
import csv


def prepare_data():
    raise NotImplementedError


def predict(input_model: Sequential, input_image, target_size):
    # predicting multiple images at once
    img = image.load_img(
        input_image,
        target_size=(target_size, target_size),
        # color_mode='grayscale' # comment this line if you see anything like ValueError: Error when checking input: expected conv2d_1_input to have shape (500, 500, 3) but got array with shape (500, 500, 1)
    )
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)

    # pass the list of multiple images np.vstack()
    images = np.vstack([x])
    output = input_model.predict(images)

    """
    predictions = model.predict_generator(self.test_generator)
    predictions = np.argmax(predictions, axis=-1) #multiple categories

    label_map = (train_generator.class_indices)
    label_map = dict((v,k) for k,v in label_map.items()) #flip k,v
    predictions = [label_map[k] for k in predictions]
    """
    return output


def folder(input_model: str, input_folder: str, target_size: int):
    """
    Folder version of the prediction function provided above.
    :param input_model:
    :param input_folder:
    :param target_size:
    :return:
    """

    # Load model
    assert os.path.exists(input_model)
    loaded_model = load_model(input_model)

    # Load files that predictions will be run upon.
    assert os.path.exists(input_folder)
    files = recursive_list_re(input_folder, "png$")

    # Place holder list of the actual prediction results to be returned.
    prediction_list = []

    # Go through all files.
    for file in tqdm(files):
        output = predict(loaded_model, file, target_size)
        # print(output[0])
        prediction_list.append(output[0])

    return prediction_list


def write_regression_prediction(
    model_path: str, input_folder: str, output_csv_path: str
):
    """
    Predict use the model provided on the input data provided. Write to CSV.
    :param model_path:
    :param input_folder:
    :return:
    """

    list_files = recursive_list_re(input_folder, "png$")
    list_predictions = folder(model_path, input_folder, 500)
    assert len(list_files) == len(list_predictions)

    # Open files for writing.
    with open(output_csv_path, "w", newline="") as csv_file:
        # Open file
        csv_writer = csv.DictWriter(
            csv_file, fieldnames=["File_Name", "r0", "r1", "r2"]
        )
        csv_writer.writeheader()

        for index in range(len(list_files)):
            # Build tuple from the file path and the prediction
            case = {
                "File_Name": list_files[index],
                "r0": list_predictions[index][0],  # for regression tasks
                "r1": list_predictions[index][1],  # for regression tasks
                "r2": list_predictions[index][2],  # for regression tasks
            }
            # Write to CSV
            csv_writer.writerow(case)


def test_regression():

    folder_path = (
        r"C:\GitHub\MarkerTrainer\data_augmented\merged\2018-09-30T16_55_28.587977500px"
    )
    csv_truth_path = r"C:\GitHub\MarkerTrainer\results\regression_truth.csv"
    csv_prediction_path = r"C:\GitHub\MarkerTrainer\results\regression.csv"

    # Generate the list of ground truth based on heuristic algirthms
    generate_csv(folder_path, csv_truth_path)

    # Write the regression into a CSV
    write_regression_prediction(
        r"C:\GitHub\MarkerTrainer\models\2018-10-28T12_31_37.801331",
        folder_path,
        csv_prediction_path,
    )


if __name__ == "__main__":
    test_regression()
