from model.Marker_PresenceClassification.CNN3_1080_class import RegressionCNNFor1080Ti


from model.cleanup import cleanLog

# This is the main script entry point to invoke the CNN.

# By importing from different class as model, they can be invoked individually here.
cleanLog(None)
image_size = 500
output_channel = 3

# Model creation:
model1 = RegressionCNNFor1080Ti(image_size, output_channel)
model1.create()
model1.compile()
model1.load_data()
model1.run()
