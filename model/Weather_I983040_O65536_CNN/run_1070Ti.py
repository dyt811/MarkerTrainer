from model.I983040_O1_CNN.HighDimensionalTo2D import CNN_I983040_O1_R


from model.cleanup import cleanLog

# This is the main script entry point to invoke the CNN.

# By importing from different class as model, they can be invoked individually here.
# Setup:
input_shape = (256, 256, 19)
output_channel = (256, 256, 1)
batch_size = 32
size_step = 128
size_epoch = 500


# Model creation:
model1 = CNN_I983040_O1_R(input_shape, output_channel)
model1.create()
model1.compile()
model1.IOCheck()
model1.load_data(r"C:\Temp\WeatherChallenge\data", batch_size)
model1.run(size_step, size_epoch)
