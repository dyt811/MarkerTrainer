import os


from keras.models import Model
from keras.models import Sequential
from keras.layers import (
    Dense,
    Dropout,
    Activation,
    BatchNormalization,
    Conv2D,
    MaxPooling2D,
    Flatten,
    Reshape,
    UpSampling2D,
    Input,
    Dense,
    LeakyReLU,
)
from keras.layers import LeakyReLU, concatenate
from keras.callbacks import TensorBoard, ModelCheckpoint

from PythonUtils.PUFile import unique_name
from PythonUtils.PUFolder import get_abspath, create

from model.abstract import CNN_model
from model.path import get_paths
from model.stage import stage

from generator.Weather_DataSequence7_TimeStamp import DataSequence, ProcessingMode


class CNN_I720896_O65536_R_Temporal(CNN_model):

    """
    This model is used to conver the 19 dimensional data which include temperature information for a given spatial temporal combination in the weather challenge:
    Input: 19 variable (rough temperature in the low resolution region upsampled to 256 to 256 regions with high resolution 256 by 256 x 14 other weather related measurement.
    Output: 1 Variable (temperature)
    Output Type: Regression
    """

    def __init__(self, input_shape, output_classes):

        self.input_shape = input_shape
        self.output_classes = output_classes

        self.train_data = None
        self.validation_data = None
        self.callbacks_list = None

        self.model = None

        self.path_model = None
        self.path_prediction = None
        self.model_stage = stage.Initialized

    def create(self):
        """
        This model is optimizd for REGRESSION type of network using a functional API 2019-06-24T140401EST
        :param input_shape:
        :param output_classes:
        :return:
        """

        # Primary weather data input:
        primary_input = Input(shape=self.input_shape, name="primary_input")

        # First convolution block
        conv1 = Conv2D(padding="same", strides=(1, 1), kernel_size=(3, 3), filters=16)(
            primary_input
        )
        bn1 = BatchNormalization()(conv1)
        lrl1 = LeakyReLU(0.1)(bn1)
        # MaxPool it
        mp1 = MaxPooling2D(pool_size=(2, 2))(lrl1)
        # conv1 = Dropout(rate=0.2)(conv1)

        conv2 = Conv2D(padding="same", strides=(1, 1), kernel_size=(2, 2), filters=16)(
            mp1
        )
        bn2 = BatchNormalization()(conv2)
        lrl2 = LeakyReLU(0.1)(bn2)
        mp2 = MaxPooling2D(pool_size=(2, 2))(lrl2)
        dr2 = Dropout(rate=0.2)(mp2)

        conv3 = Conv2D(padding="same", strides=(1, 1), kernel_size=(2, 2), filters=8)(
            dr2
        )
        bn3 = BatchNormalization()(conv3)
        lrl3 = LeakyReLU(0.1)(bn3)
        mp3 = MaxPooling2D(pool_size=(2, 2))(lrl3)
        dr3 = Dropout(rate=0.2)(mp3)

        conv4 = Conv2D(padding="same", strides=(1, 1), kernel_size=(2, 2), filters=4)(
            dr3
        )
        bn4 = BatchNormalization()(conv4)
        lrl4 = LeakyReLU(0.1)(bn4)
        mp4 = MaxPooling2D(pool_size=(2, 2))(lrl4)
        dr4 = Dropout(rate=0.2)(mp4)

        conv5 = Conv2D(padding="same", strides=(1, 1), kernel_size=(2, 2), filters=4)(
            dr4
        )
        bn5 = BatchNormalization()(conv5)
        lrl5 = LeakyReLU(0.2)(bn5)
        mp5 = MaxPooling2D(pool_size=(2, 2))(lrl5)
        dr5 = Dropout(rate=0.25)(mp5)

        # Flatten the output to enable the merge to happen with the other input
        first_part_output = Flatten()(dr5)

        # Secondary date information input: the date information.
        secondary_input = Input(shape=(2,), name="secondary_input")

        # Concate, dense activation, batch normalize, reshape, upsample
        concatF = concatenate([first_part_output, secondary_input])
        denseF = Dense(16384)(concatF)
        bnF = BatchNormalization()(denseF)
        acF = LeakyReLU(0.2)(bnF)
        drF = Dropout(rate=0.25)(acF)
        rsF = Reshape((128, 128, 1))(drF)
        usF = UpSampling2D(size=(2, 2))(rsF)

        self.model = Model([primary_input, secondary_input], usF)
        self.stage = stage.Created

        return self.model

    def IOCheck(self):
        import numpy as np

        dummy_input = np.ones((11, 256, 256))
        preds = self.model.predict(dummy_input)
        print(preds.shape)

    def compile(self):
        """
        This is a model specific compilation process, must choosen/update based on the purpose of the network.
        :param model:
        :return:
        """
        self.model.compile(
            loss="mse", optimizer="adadelta", metrics=["mae", "mse", "mape", "cosine"]
        )
        self.model.summary()
        self.stage = stage.Compiled
        return self.model

    def load_data(self, data_path, batch_size=5):
        """
        Load the data specification from the path in the .env.
        """

        # Dynamically generate model input_path.
        this_file = os.path.realpath(__file__)
        project_root = get_abspath(this_file, 2)

        # Log path.
        path_log, self.path_model = get_paths(project_root)

        # Log run path.
        path_log_run = os.path.join(path_log, unique_name() + __name__)

        # Create the Log run path.
        create(path_log_run)

        # Path to the train.
        path_train_spec = r"E:\WeatherData\data\stamped"

        # Generate data sequence.
        train_data = DataSequence(
            path_train_spec, batch_size, mode=ProcessingMode.Train
        )

        # Path to the validation.
        path_validate_spec = r"E:\WeatherData\data\stamped"

        # Generate data sequence.
        validation_data = DataSequence(
            path_validate_spec, batch_size, mode=ProcessingMode.Validation
        )

        # Model name.
        model_name = os.path.join(self.path_model, f"{unique_name()}_{__name__}")

        callback_save_model = ModelCheckpoint(
            model_name,
            monitor="val_mean_absolute_error",
            verbose=1,
            save_best_only=True,
            mode="min",
        )

        # Generate the tensorboard
        callback_tensorboard = TensorBoard(
            log_dir=path_log_run, histogram_freq=0, write_images=True
        )

        self.callbacks_list = [callback_tensorboard, callback_save_model]
        self.stage = stage.DataLoaded
        self.train_data = train_data
        self.validation_data = validation_data

    def run(self, size_step=256, size_epoch=500):
        self.model.fit_generator(
            self.train_data,
            steps_per_epoch=size_step,
            epochs=size_epoch,
            validation_data=self.validation_data,
            validation_steps=size_step,
            callbacks=self.callbacks_list,
        )
        self.path_prediction = os.path.join(self.path_model, unique_name() + ".h5")
        self.model.save(self.path_prediction)
        self.stage = stage.Ran
        return self.path_prediction
