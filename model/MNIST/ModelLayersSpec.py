from keras.models import Sequential
from keras.layers import (
    Dense,
    Conv2D,
    MaxPooling2D,
    Dropout,
    Flatten,
    BatchNormalization,
    LeakyReLU,
    Softmax,
)


def define_CNN_model(input_shape, output_classes=10, leak_alpha=0.25):
    model = Sequential()
    model.add(
        Conv2D(64, (3, 3), padding="same", strides=(1, 1), input_shape=input_shape)
    )
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.3))

    model.add(Conv2D(32, (2, 2), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.3))

    model.add(Conv2D(32, (2, 2), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.3))

    model.add(Conv2D(32, (2, 2), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.3))

    model.add(Flatten())
    model.add(Dense(128))
    model.add(BatchNormalization())
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(Dense(64))
    model.add(BatchNormalization())
    model.add(LeakyReLU(alpha=leak_alpha))
    model.add(Dropout(rate=0.5))
    model.add(Dense(output_classes, activation="softmax"))
    return model
