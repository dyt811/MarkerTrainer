from keras.preprocessing import image
import os
import numpy as np

# from sklearn.metrics import confusion_matrix
from keras.models import load_model
from PythonUtils.PUFolder import recursive_list
from PythonUtils.PUFile import unique_name
from PythonUtils.PUJson import write_json
from model.MNIST.ModelLayersSpec import define_CNN_model
import csv
from tqdm import tqdm
from pathlib import Path
from keras.utils import to_categorical
from pycm import *

import logging

logger = logging.getLogger("PredictionProcess")
logging.basicConfig(level=logging.INFO)


class model_prediction:
    def __init__(
        self,
        path_model=None,
        path_model_weights=None,
        shape_input=(28, 28, 1),
        classes_output=10,
        threshold=0,  # below which, uncertain label is applied.
        threshold_certainties=None,
    ):
        # Instantiate the model based on what parameters was instantiated.
        if path_model_weights is not None:
            # Load model
            assert os.path.exists(path_model_weights)
            self.input_model = define_CNN_model(
                input_shape=shape_input, output_classes=classes_output
            )
            self.input_model.load_weights(path_model_weights)
            self.path_model_info = path_model_weights
        elif path_model is not None:
            # Load model
            assert os.path.exists(path_model)
            self.input_model = load_model(path_model)
            self.path_model_info = path_model
        else:
            return

        if threshold_certainties is None:
            self.threshold_certainties = [
                0.99,  # for 0
                0.9999,  # for 1
                0.99,  # for 2
                0.99,  # for 3
                0.99,  # for 4
                0.99,  # for 5
                0.999,  # for 6
                0.999,  # for 7
                0.6,  # for 8
                0.999,  # for 9
            ]
        self.global_threshold = threshold
        self.results_prediction = []
        self.results_prediction_onehot = None

    def predict_image(self, path_input_image):
        """
        Use the provided model weight to predice the image segmentation output
        :param path_input_model_weight:
        :param path_input_image:
        :return:
        """

        # predicting multiple images at once
        img = image.load_img(
            path_input_image,
            grayscale=True
            # target_size=(target_size, target_size), # no target size as we are not downsampling.
        )
        x = image.img_to_array(img)
        x = np.expand_dims(
            x, axis=0
        )  # pad the x array dimension to conform to 4D tensor.

        # pass the list of multiple images np.vstack()
        images = np.vstack([x])
        output = self.input_model.predict(images)
        logger.info(output)
        np.argmax(output, axis=0)
        logger.info(output)

    def de_categorical(self):
        """
        This function converts the one hot vector into class indicator filtering for global probabilities lower than the threshold_certainty
        :return:
        """

        list_categorical = []
        for row_onehot_encoded in self.results_prediction_onehot:
            # Class with the highest probability.
            class_number = np.argmax(row_onehot_encoded)

            # If the max one hot value is less than the threshold_certainty, set as unknown.
            if max(row_onehot_encoded) < self.threshold_certainties[class_number]:
                list_categorical.append(99)
            else:
                list_categorical.append(class_number)
        return np.array(list_categorical)

    def de_categorical_global(self):
        """
        This function converts the one hot vector into class indicator filtering for global probabilities lower than the threshold_certainty
        :return:
        """

        list_categorical = []
        for row_onehot_encoded in self.results_prediction_onehot:
            # Class with the highest probability.
            class_number = np.argmax(row_onehot_encoded)

            # If the max one hot value is less than the threshold_certainty, set as unknown.
            if max(row_onehot_encoded) < self.global_threshold:
                list_categorical.append(99)
            else:
                list_categorical.append(class_number)
        return np.array(list_categorical)

    def generate_confusion_matrix(self, path_groundtruth: str):
        """
        Generate the confusion matrix based on latest prediction output and the ground truth.
        :param path_groundtruth:
        :return:
        """
        # self.results_prediction = np.argmax(self.results_prediction_onehot, axis=1, out=None)
        self.results_prediction = self.de_categorical_global()
        # Get ground truth from the CSV input, assuming no title etc.

        self.ground_truth = np.loadtxt(
            open(str(path_groundtruth), "rb"), delimiter=",", skiprows=0, dtype=np.int16
        )
        # self.ground_truth_onehot = to_categorical(self.ground_truth, num_classes=10)

        # To compare, they must have the same length.
        assert len(self.ground_truth) == len(self.results_prediction)

        # Compute confusion matrix using SKLearn
        CM_mnist = ConfusionMatrix(
            actual_vector=self.ground_truth, predict_vector=self.results_prediction
        )
        path_file = str(
            Path(path_groundtruth).parent.parent
            / f"{unique_name()}_MNIST_ConfusionMatrix"
        )
        CM_mnist.save_csv(path_file)
        CM_mnist.save_html(path_file)
        CM_mnist.save_obj(path_file)
        CM_mnist.save_stat(path_file)

        logger.info("PyCM confusion matrix generated and saved")

    def predict_folder(self, input_folder: str):
        """
        Folder version of the prediction function provided above.
        :param input_model:
        :param input_folder:
        :param target_size:
        :return:
        """

        # Load files that predictions will be run upon.
        assert os.path.exists(input_folder)

        list_files = recursive_list(input_folder)
        list_prediction = []
        for file in tqdm(list_files):

            # skip if it is not an image. e.g. previously generated CSVs etc.
            if (
                "JPEG" not in file.upper()
                and "PNG" not in file.upper()
                and "JPEG" not in file.upper()
            ):
                continue

            # predicting multiple images at once
            img = image.load_img(file, color_mode="grayscale")

            x = image.img_to_array(img)
            x = np.expand_dims(
                x, axis=0
            )  # pad the x array dimension to conform to 4D tensor.

            # pass the list of multiple images np.vstack()
            images = np.vstack([x])
            output = self.input_model.predict(images)

            list_prediction.append(output)
            self.results_prediction_onehot = (
                output
                if self.results_prediction_onehot is None
                else np.vstack((self.results_prediction_onehot, output))
            )

        path_csv = Path(input_folder) / f"{unique_name()}-MNIST_Prediction.csv"

        # Open files for writing.
        with open(path_csv, "w", newline="") as csv_file:

            # Header Field:
            # ["0", 1", "2", "3", ... "9"]
            header = ["order"]
            header.extend(list(map(str, list(range(10)))))

            # Open file
            csv_writer = csv.DictWriter(csv_file, fieldnames=header)
            # Write header.
            csv_writer.writeheader()

            # keep track of the order for easier readability.
            order = 0
            # Write per row
            for prediction in list_prediction:
                dict = {}
                dict["order"] = order
                # For each categorical prediction, write the probabilities.
                for index, probability in enumerate(prediction[0]):
                    dict[str(index)] = probability
                # Write to CSV
                csv_writer.writerow(dict)
                order = order + 1

        # Write JSON record for easier debug.
        write_JSON_records(self.path_model_info, list_files, input_folder)

        logger.info("Test folder prediction completed. ")


def write_JSON_records(path_model, list_images, destination):
    """
    Keep a simple JSON record of where the model came from
    :param path_model:
    :return:
    """
    data = {}
    data["model"] = path_model
    data["images"] = list_images
    path_json = Path(destination) / (unique_name() + "_prediction_details.json")
    write_json(path_json, data)
    logger.info("JSON record written for the prediction process.")


if __name__ == "__main__":

    # test.predict_image(r"C:\BatchModelTesting\IOTest\00000.jpeg")
    # for hold in [0.6, 0.75, 0.9, 0.95, 0.99, 0.999, 0.9999, 0.99999, 0.999999]:
    test = model_prediction(
        path_model_weights=r"C:\Git\MarkerTrainer\models\2019-09-29T16_09_18.716190_Weights_model.MNIST.ModelClassSpec.h5"
    )
    test.predict_folder("C:\Git\MarkerTrainer\data_mnist\holdout_train")
    test.generate_confusion_matrix(
        "C:\Git\MarkerTrainer\data_mnist\holdout_label\label.csv"
    )
# test.predict_folder("C:\BatchModelTesting\IOTest")
# test.generate_confusion_matrix("C:\BatchModelTesting\IOTest\label.csv")
