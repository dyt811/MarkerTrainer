from model.MNIST.ModelClassSpec import CNN_I2D_O1D
from pathlib import Path
from lossfn.ssim import (
    loss_SSIM,
    loss_mae_diff_SSIM_composite,
    loss_mse_diff_SSIM_composite,
)
from lossfn.f1 import f1_metric

"""
# This is the main class to run MULTI class training for ServeStall. Labelled data ideally should be in grayscale label 1200 x 255 x 3  
"""
# By importing from different class as model, they can be invoked individually here.
# Setup:
batch_size = 64  # images PER epoch

input_shape = (
    28,  # height first
    28,  # width later.
    1,  # color channel
)  # Train images are RGB. first number is the number of rows (y positions), second number is the number of columns (x)

num_classes = (
    10
)  # keep this at one, because the ground truth label is not using one hot encoding. Prepare the data accordingly and using gray scale to encode class instead. Pretty much nothing else here needs to be modified.
size_step = 128
size_epoch = 100

train_data_path = Path(
    r"C:/Git/MarkerTrainer/data_mnist/"
)  # this folder MUST contain a LABEL folder and a TRAIN folder of flat images WITH IDENTICAL NAME-label pair.

# Model creation:
model_multi_class = CNN_I2D_O1D(
    input_shape=input_shape,
    output_classes=num_classes,
    train_data_path=train_data_path,
    loss="categorical_crossentropy",
    metrics=["mae", "mse", "mape", "cosine", "accuracy"],
    checkpoint_metric="val_acc",
)

model_multi_class.create()
model_multi_class.compile()
model_multi_class.IOCheck()
model_multi_class.load_data(batch_size=batch_size)
model_multi_class.run(size_step, size_epoch)
final_model1, final_model1_weights = model_multi_class.run(size_step, size_epoch)

# Update this folder path to the folder contain HOLDOUT images.
model_multi_class.predict(
    final_model1_weights, r"C:\Git\MarkerTrainer\data_mnist\holdout_train"
)
