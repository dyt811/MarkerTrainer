import os
from PythonUtils.PUFolder import recursive_list
from PythonUtils.PUFile import filelist_delete


def cleanLog(input_path):
    """
    Clean the log directory of TensorBoard
    :param input_path:
    :return:
    """
    if input_path is None:
        # Dynamicly generate model input_path.
        project_root = os.path.realpath(__file__)
        log_path = os.path.join(os.path.dirname(project_root), "logs")
    else:
        log_path = input_path

    if os.path.exists(log_path):
        files = recursive_list(log_path)
        if len(files) > 0:
            filelist_delete(files)
