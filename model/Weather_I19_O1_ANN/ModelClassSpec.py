import os

from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization
from keras.layers import LeakyReLU
from keras.callbacks import TensorBoard, ModelCheckpoint

from PythonUtils.PUFile import unique_name
from PythonUtils.PUFolder import get_abspath, create

from model.abstract import CNN_model
from model.path import get_paths
from model.stage import stage

from generator.Weather_DataSequence4_AddTemporalRecodedSpatial import (
    DataSequence,
    ProcessingMode,
)


class SimpleANN_I19_O1_R(CNN_model):

    """
    This model is used to conver the 19 dimensional data which include temperature information for a given spatial temporal combination in the weather challenge:
    Input: 19 variable (rough temperature in the low resolution region upsampled to 256 to 256 regions with high resolution 256 by 256 x 14 other weather related measurement.
    Output: 1 Variable (temperature)
    Output Type: Regression
    """

    def __init__(self, input_shape, output_classes):

        self.input_shape = input_shape
        self.output_classes = output_classes

        self.train_data = None
        self.validation_data = None
        self.callbacks_list = None

        self.model = None

        self.path_model = None
        self.path_prediction = None
        self.model_stage = stage.Initialized

    def create(self):
        """
        This model is optimizd for REGRESSION type of network no a
        :param input_shape:
        :param output_classes:
        :return:
        """
        model = Sequential()

        model.add(Dense(16, input_shape=self.input_shape))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.1))
        model.add(Dropout(rate=0.25))

        model.add(Dense(8))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.1))
        model.add(Dropout(rate=0.25))

        model.add(Dense(4))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.1))
        model.add(Dropout(rate=0.25))

        model.add(
            Dense(
                1,
                bias_initializer="normal",
                kernel_initializer="normal",
                activation="linear",
            )
        )

        self.model = model
        self.stage = stage.Created
        return model

    def compile(self):
        """
        This is a model specific compilation process, must choosen/update based on the purpose of the network.
        :param model:
        :return:
        """
        self.model.compile(
            loss="mape", optimizer="adadelta", metrics=["mae", "mse", "mape", "cosine"]
        )
        self.model.summary()
        self.stage = stage.Compiled
        return self.model

    def load_data(self, data_path, batch_size=1048576):
        """
        Load the data specification from the path in the .env.
        """

        # Dynamically generate model input_path.
        this_file = os.path.realpath(__file__)
        project_root = get_abspath(this_file, 2)

        # Log path.
        path_log, self.path_model = get_paths(project_root)

        # Log run path.
        path_log_run = os.path.join(path_log, unique_name())

        # Create the Log run path.
        create(path_log_run)

        # Path to the train.
        path_train_spec = fr"{data_path}/dataset_train.txt"

        # Generate data sequence.
        train_data = DataSequence(path_train_spec, 1048576, mode=ProcessingMode.Train)

        # Path to the validation.
        path_validate_spec = fr"{data_path}/dataset_validate.txt"

        # Generate data sequence.
        validation_data = DataSequence(
            path_validate_spec, 1048576, mode=ProcessingMode.Validation
        )

        # Model name.
        model_name = os.path.join(self.path_model, f"{unique_name()}_{__name__}")

        callback_save_model = ModelCheckpoint(
            model_name,
            monitor="val_mean_absolute_error",
            verbose=1,
            save_best_only=True,
            mode="min",
        )

        # Generate the tensorboard
        callback_tensorboard = TensorBoard(
            log_dir=path_log_run, histogram_freq=0, write_images=True
        )

        self.callbacks_list = [callback_tensorboard, callback_save_model]
        self.stage = stage.DataLoaded
        self.train_data = train_data
        self.validation_data = validation_data

    def run(self, size_step=256, size_epoch=500):
        self.model.fit_generator(
            self.train_data,
            steps_per_epoch=size_step,
            epochs=size_epoch,
            validation_data=self.validation_data,
            validation_steps=size_step,
            callbacks=self.callbacks_list,
        )
        self.path_prediction = os.path.join(self.path_model, unique_name() + ".h5")
        self.model.save(self.path_prediction)
        self.stage = stage.Ran
        return self.path_prediction

    def predict(self, input_model: Sequential, input_image, target_size):
        # predicting multiple images at once
        img = image.load_img(
            input_image,
            target_size=(target_size, target_size),
            # color_mode='grayscale' # comment this line if you see anything like ValueError: Error when checking input: expected conv2d_1_input to have shape (500, 500, 3) but got array with shape (500, 500, 1)
        )
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)

        # pass the list of multiple images np.vstack()
        images = np.vstack([x])
        output = input_model.predict(images)

        """
        predictions = model.predict_generator(self.test_generator)
        predictions = np.argmax(predictions, axis=-1) #multiple categories

        label_map = (train_generator.class_indices)
        label_map = dict((v,k) for k,v in label_map.items()) #flip k,v
        predictions = [label_map[k] for k in predictions]
        """
        return output
