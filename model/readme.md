#/model Folder: 
This is where variety of past Keras models were built. Each folder represents a particular class/archetype of convolutional neural network attempted. 

Each subdirectory contain a model architecture used in the past for particular competitions. See their respective README for more details.  

##Typical contents you will find in the architecture. 

### /jupyter_check_XXX.ipynb: 
* used to check data from a compeittion. 

### /ModelClassSpec.py:
* the class representation of the model. Usually also contain the details of the functions that the model offer, eg: 

###/ModelLayersSpec.py
* the core of the model, which contain all the details related to the custom layers and detailed layers construction 

###/run_XXXXX.py
* this is the script which instanticate the ModelClassSpec, which itself instanticates the necessary ModelLayerSpecs. 
* this is the key entry point to start everything and should be the first thing to be modified. 
    
###/predict_XXXXXX.py
# used to load the data and achieve predition. 