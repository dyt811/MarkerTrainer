### Simple convolutional neural network to detect presence


* This is a simple network to do very specific object detection after being training. 
* It was primarily used during MotionCorrect 2017/2018 to detect a 2D flat markers.   