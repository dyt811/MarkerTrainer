import os

from keras.models import Sequential
from keras.layers import (
    Dense,
    Conv2D,
    MaxPooling2D,
    Dropout,
    Flatten,
    BatchNormalization,
    LeakyReLU,
)
from keras.callbacks import TensorBoard, ModelCheckpoint

from PythonUtils.PUFile import unique_name
from PythonUtils.env import load_dotenv_var

from model.abstract import CNN_model
from model.path import get_paths, get_project_root
from model.stage import stage

from generator.csvgen import generate_csv
from generator.Marker_DataSequence_Poses import DataSequence


class RegressionCNNFor1080Ti(CNN_model):

    """
    The sole responsibility of this function is to generate a VERY specific network, SUCH that its create_network function can be easily swapped in via import by the parental function.  
    """

    def __init__(self, input_shape, output_classes):
        self.input_shape = input_shape
        self.output_classes = output_classes

        self.train_data = None
        self.validation_data = None
        self.callbacks_list = None

        self.model = None

        self.path_model = None
        self.path_prediction = None
        self.model_stage = stage.Initialized

    def create(self):
        """
        This model is optimizd for REGRESSION type of network no a
        :param input_shape:
        :param output_classes:
        :return:
        """
        model = Sequential()
        model.add(
            Conv2D(
                16,
                (5, 5),
                padding="same",
                strides=(2, 2),
                input_shape=(self.input_shape, self.input_shape, 3),
            )
        )
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # model.add(Conv2D(16, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        model.add(Conv2D(16, (3, 3), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # model.add(Conv2D(32, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        model.add(Conv2D(32, (3, 3), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # #model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))
        #
        model.add(Conv2D(32, (3, 3), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # #model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))
        #
        model.add(Conv2D(64, (3, 3), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # #model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))
        #
        model.add(Conv2D(64, (3, 3), padding="same", strides=(1, 1)))
        model.add(BatchNormalization(axis=-1))
        model.add(LeakyReLU(alpha=0.2))
        # model.add(Conv2D(64, (5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.2))

        model.add(Flatten())
        # model.add(Dense(2048))
        # model.add(LeakyReLU(alpha=0.1))
        model.add(Dense(1024))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.1))
        model.add(Dense(512))
        model.add(BatchNormalization())
        model.add(LeakyReLU(alpha=0.1))
        model.add(Dropout(0.5))
        model.add(Dense(self.output_classes))

        self.model = model
        self.stage = stage.Created
        return model

    def compile(self):
        """
        This is a model specific compilation process, must choosen/update based on the purpose of the network.
        :param model:
        :return:
        """
        self.model.compile(
            loss="mean_squared_error", optimizer="adadelta", metrics=["acc", "mae"]
        )
        self.stage = stage.Compiled
        return self.model

    def load_data(self):
        """
        Load the data specification from the path in the .env.
        """

        train_path = load_dotenv_var("train_path")
        train_csv_path = load_dotenv_var("train_csv_path")

        validate_path = load_dotenv_var("validate_path")
        validate_csv_path = load_dotenv_var("validate_csv_path")

        # Dynamicly generate model input_path.
        project_root = get_project_root()
        path_log, self.path_model = get_paths(project_root)

        generate_csv(train_path, train_csv_path)
        train_data = DataSequence(train_csv_path, 128, mode="Train")

        generate_csv(validate_path, validate_csv_path)
        validation_data = DataSequence(validate_csv_path, 128)

        model_name = os.path.join(self.path_model, unique_name())

        callback_save_model = ModelCheckpoint(
            model_name, monitor="val_acc", verbose=1, save_best_only=True, mode="max"
        )

        # Generate the tensorboard
        callback_tensorboard = TensorBoard(
            log_dir=path_log, histogram_freq=0, write_images=True
        )

        callbacks_list = [callback_tensorboard, callback_save_model]
        self.stage = stage.DataLoaded
        return train_data, validation_data, callbacks_list

    def run(self):
        self.model.fit_generator(
            self.train_data,
            steps_per_epoch=256,
            epochs=500,
            validation_data=self.validation_data,
            validation_steps=256,
            callbacks=self.callbacks_list,
        )
        self.path_prediction = os.path.join(self.path_model, unique_name() + ".h5")
        self.model.save(self.path_prediction)
        self.stage = stage.Ran
        return self.path_prediction
