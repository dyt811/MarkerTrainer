from keras.models import Sequential
from keras.layers import (
    Dense,
    Conv2D,
    MaxPooling2D,
    Dropout,
    Flatten,
    BatchNormalization,
)
from keras.layers import LeakyReLU

"""
The sole responsibility of this function is to generate a VERY specific network, SUCH that its create_network function can be easily swapped in via import by the parental function.  
"""


def create_model(input_shape, output_classes):
    """
    This model is optimizd for REGRESSION type of network no a
    :param input_shape:
    :param output_classes:
    :return:
    """
    model = Sequential()
    model.add(
        Conv2D(
            16,
            (5, 5),
            padding="same",
            strides=(2, 2),
            input_shape=(input_shape, input_shape, 3),
        )
    )
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Conv2D(16, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))

    model.add(Conv2D(16, (3, 3), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Conv2D(32, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))

    model.add(Conv2D(32, (3, 3), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # #model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))
    #
    model.add(Conv2D(32, (3, 3), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # #model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))
    #
    model.add(Conv2D(64, (3, 3), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # #model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))
    #
    model.add(Conv2D(64, (3, 3), padding="same", strides=(1, 1)))
    model.add(BatchNormalization(axis=-1))
    model.add(LeakyReLU(alpha=0.2))
    # model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(rate=0.2))

    model.add(Flatten())
    # model.add(Dense(2048))
    # model.add(LeakyReLU(alpha=0.1))
    model.add(Dense(1024))
    model.add(BatchNormalization())
    model.add(LeakyReLU(alpha=0.1))
    model.add(Dense(512))
    model.add(BatchNormalization())
    model.add(LeakyReLU(alpha=0.1))
    model.add(Dropout(0.5))
    model.add(Dense(output_classes))

    return model


def compile_model(model: Sequential):
    """
    This is a model specific compilation process, must choosen/update based on the purpose of the network.
    :param model:
    :return:
    """
    model.compile(
        loss="mean_squared_error", optimizer="adadelta", metrics=["acc", "mae"]
    )
