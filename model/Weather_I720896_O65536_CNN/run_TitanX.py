from model.I720896_O65536_CNN.HighDimensionalTo2D import CNN_I720896_O65536_R


from model.cleanup import cleanLog

# This is the main script entry point to invoke the CNN.

# By importing from different class as model, they can be invoked individually here.
# Setup:
batch_size = 16
input_shape = (256, 256, 11)  # this is the input shape of the FINAL TRIMMed model.
output_channel = (256, 256, 1)

size_step = 128
size_epoch = 500


# Model creation:
model1 = CNN_I720896_O65536_R(input_shape, output_channel)
model1.create()
model1.compile()
# model_single_class.IOCheck()
model1.load_data(r"E:\WeatherData\data\stamped", batch_size)
model1.run(size_step, size_epoch)
