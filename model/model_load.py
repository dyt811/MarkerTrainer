from keras.models import load_model as keras_load_model
from model.abstract import CNN_model


def load_model_weights(weight_path, model1: CNN_model):
    """
    Sometimes, when models were saved with custom layers, you need to instantiate the model first and then load the model weights intead of loading the model all in one go.
    :param weight_path:
    :param model1:
    :return:
    """
    raise NotImplementedError
    # Create the model first:
    recreated_model = model1.create()
    # Load the weight.
    recreated_model.load_weight(weiht_path)


def load_model(model_path: Path):
    """
    Generic model loading function. Load a model from path.
    :param model_path:
    :return:
    """
    loaded_model = keras_load_model(model_path)
    return loaded_model


if __name__ == "__main__":
    model = load_model(r"C:\GitHub\MarkerTrainer\models\2018-10-12T17_34_56.150751")
