import os
import re
import numpy as np
import datetime
from PythonUtils.PUFolder import create
from PythonUtils.PUFile import unique_name


def load_npy_without_space_time():
    """
    Load the npy data and
    :return:
    """


def parse_data(input_path: str = "C:\GitHub\MarkerTrainer\data"):
    """
    Parse the date.txt to initalize the data annontation.
    :param input_path:
    :return:
    """
    date_file = "date.txt"
    os.chdir(input_path)

    file = open(date_file, "r")

    file_lines = file.readlines()

    for file_specification in file_lines:
        print(file_specification)
        regex_file_name = re.compile(r"training_\d\d\d\d_\d\d\d\d.npy")
        regex_datetime = re.compile(r"201\d-\d\d-\d\d \d\d:\d\d:\d\d")
        file_name = regex_file_name.findall(file_specification)[0]
        date_time = regex_datetime.findall(file_specification)[0]

        hour_of_the_year = date_to_nth_hour(date_time)
        print(file_name)

        x = np.load("input_" + file_name)
        # x should have 100 x 15 x 256 x 256

        y = np.load("label_" + file_name)
        # y should have 100 x 1 x 256 x 256

        x_transposed = x.transpose([2, 3, 0, 1])
        # x_transposed should have 256 by 256 by 100 by 15
        y_transposed = y.transpose([2, 3, 0, 1])
        # x_transposed should have 256 by 256 by 100 by 15

        # Gonna build a series of models:
        # Input: 15 variable
        # Output: temperature
        # N = x * y * timepoint
        # without regard to datetime, location.

        # Keeping the LAST dimension after the rearrangement:
        x_model1 = x_transposed.reshape(-1, x_transposed.shape[-1]).shape
        y_model1 = y_transposed.reshape(-1, y_transposed.shape[-1]).shape

        # x_model1 should now be 6553600, 15

        result_path = "Analyses_" + unique_name()
        create(result_path)
        os.chdir(result_path)

        # Input: 15 variable + time of the year
        # Output: temperature
        # N = x * y * timepoint
        # without regard location.

        # Input: 15 variable + time of the year + x coordinate + y coordinate.
        # Output: temperature
        # N = x * y * timepoint

        # Convert the array into: 16 variable (15 + time) > 1 outcome. for 65536 observation

        # Input: 15 variable
        # Output: temperature
        # without regard to datetime, location.

        print(date_time)


if __name__ == "__main__":
    parse_data()
