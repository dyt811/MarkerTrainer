import numpy as np
import io
from pathlib import Path
import tensorflow as tf
import os
import time
from PythonUtils.PUFile import unique_name
from PythonUtils.PUFolder import recursive_list_re, create_batch, create
import csv
from PIL import ImageDraw
import pandas as pd

"""
Adapted function to visualize the data by traversing the tensorflow records. 
"""

# This determine whether to break the TensorFlow records into class specific organization for easier single class training.
breakout_classes = True

from PIL import Image


class visualizer:
    def __init__(self, path="../data/TFRecords", output_path="../data/Output"):
        self.path = path
        self.path_save = Path(output_path + unique_name())
        self.path_csv = self.path_save.joinpath(Path("csv"))
        self.path_mask = self.path_save.joinpath(Path("mask"))
        self.path_mask_visual = self.path_save.joinpath(Path("mask_visual"))
        self.path_image = self.path_save.joinpath(Path("image"))
        self.path_segmentation = self.path_save.joinpath(Path("extraction"))
        self.path_overlay = self.path_save.joinpath(Path("overlay"))

        # Batch create these folders
        create_batch(
            [
                self.path_csv,
                self.path_mask,
                self.path_mask_visual,
                self.path_image,
                self.path_segmentation,
                self.path_overlay,
            ]
        )

        self.tfrecords = os.listdir(path)
        print(self.tfrecords)
        """
        'image/crop_height': int64_feature(crop_height),
        'image/crop_width': int64_feature(crop_width),
        'image/original_height': int64_feature(height),
        'image/original_width': int64_feature(width),
        'image/depth': int64_feature(3),
        'image/mask': bytes_feature(mask_encoded),
        'image/crop': bytes_feature(crop_encoded),
        'image/original': bytes_feature(orig_encoded),
        'image/format': bytes_feature('jpeg'.encode('utf8')),
        'label/feed': bytes_feature(feed.encode('utf8')),
        'label/product_name': bytes_feature(item_name.encode('utf8')),
        'label/shelf_start': float_feature(start),
        'label/shelf_end': float_feature(end),
        'label/id': int64_feature(item_id),
        'label/norm_id': int64_feature(norm_id),
        'label/x_coords': int64_list_feature(x_coords),
        'label/y_coords': int64_list_feature(y_coords),
        """

    def export_all(self):
        """
        Show all records from the TFrecords.
        """
        for tfrecord in self.tfrecords:
            # Exception handling.
            if tfrecord.startswith("."):
                continue
            elif tfrecord.find("tfrecord") == -1:
                continue

            # Open record iterator from teh path.
            reader = tf.python_io.tf_record_iterator(os.path.join(self.path, tfrecord))

            for serialized_example in reader:

                example = tf.train.Example()
                example.ParseFromString(serialized_example)

                try:
                    # Crop dimension
                    crop_height = example.features.feature[
                        "image/crop_height"
                    ].int64_list.value[0]
                    crop_width = example.features.feature[
                        "image/crop_width"
                    ].int64_list.value[0]

                    # Original dimension
                    original_height = example.features.feature[
                        "image/original_height"
                    ].int64_list.value[0]
                    original_width = example.features.feature[
                        "image/original_width"
                    ].int64_list.value[0]

                    # Image Color Depth.
                    depth = example.features.feature["image/depth"].int64_list.value[0]

                    image_format = (
                        example.features.feature["image/format"]
                        .bytes_list.value[0]
                        .decode("utf-8")
                    )
                    image_feed = (
                        example.features.feature["label/feed"]
                        .bytes_list.value[0]
                        .decode("utf-8")
                    )
                    product_name = (
                        example.features.feature["label/product_name"]
                        .bytes_list.value[0]
                        .decode("utf-8")
                    )

                    shelf_start = example.features.feature[
                        "label/shelf_start"
                    ].float_list.value[0]
                    shelf_end = example.features.feature[
                        "label/shelf_end"
                    ].float_list.value[0]

                    x_coords = example.features.feature[
                        "label/x_coords"
                    ].int64_list.value
                    y_coords = example.features.feature[
                        "label/y_coords"
                    ].int64_list.value

                    # Zipped to list of tuples for Draw Function
                    xy_coords = list(zip(x_coords, y_coords))

                    item_id = example.features.feature["label/id"].int64_list.value[0]
                    norm_id = example.features.feature[
                        "label/norm_id"
                    ].int64_list.value[0]

                    # Set a standarized name for the record instance.
                    record_name = unique_name()

                    # Create class specific locations so the next save operations won't fail.

                    classID = str(norm_id)
                    # Aggregate the data and write out to files so I can read them better.
                    aggregated_data = [
                        record_name + image_format,
                        image_feed,
                        product_name,
                        shelf_start,
                        shelf_end,
                        x_coords,
                        y_coords,
                        item_id,
                        norm_id,
                    ]

                    if breakout_classes:
                        self.create_class_folder(norm_id)
                        path_csv_file = Path(
                            os.path.join(self.path_csv, classID, record_name + ".csv")
                        )
                    else:
                        path_csv_file = Path(
                            os.path.join(self.path_csv, record_name + ".csv")
                        )

                    with open(path_csv_file, mode="w") as csvFile:
                        writer = csv.writer(csvFile)
                        writer.writerow(aggregated_data)

                    # Read the raw mask bytes and show them.
                    img_mask_bytes = example.features.feature[
                        "image/mask"
                    ].bytes_list.value[0]
                    imgByteArr = io.BytesIO()
                    img_mask = Image.open(io.BytesIO(img_mask_bytes))
                    # img_mask.show()

                    # Read into numpy, paint with (255 - NORMID), convert to PIL, save.
                    # 255 - NORMID because some NORMID = 0. Cannot just paint directly with NORMID
                    image_mask_raw = np.array(img_mask)
                    image_mask_painted = image_mask_raw * (255 - norm_id)
                    image_mask_final = Image.fromarray(image_mask_painted)
                    # img_mask.save(os.path.join(self.path_mask, classID, f"{record_name}.{image_format}"))

                    # Read the crop bytes and show them.
                    img_crop_bytes = example.features.feature[
                        "image/crop"
                    ].bytes_list.value[0]
                    imgByteArr = io.BytesIO()
                    img_crop = Image.open(io.BytesIO(img_crop_bytes))
                    # img_crop.show()

                    # Read the original and show them.
                    img_overlay_bytes = example.features.feature[
                        "image/original"
                    ].bytes_list.value[0]
                    imgByteArr = io.BytesIO()
                    img_overlay = Image.open(io.BytesIO(img_overlay_bytes))
                    # img_overlay.show

                    # Record name:
                    filename_extension = f"{record_name}.{image_format}"

                    # Write out to /image BEFORE applying the overlay.
                    if breakout_classes:
                        img_overlay.save(
                            os.path.join(self.path_image, classID, filename_extension)
                        )
                    else:
                        img_overlay.save(
                            os.path.join(self.path_image, filename_extension)
                        )

                    # Draw the red polygon overlay of the area labelled.
                    draw = ImageDraw.Draw(img_overlay)
                    draw.polygon(xy_coords, fill=(255, 0, 0, 128))

                    if (
                        breakout_classes
                    ):  # when break into class ID folders, path is slightly different.
                        # Save mask (0 to 1)
                        img_mask.save(
                            os.path.join(self.path_mask, classID, filename_extension)
                        )
                        # Save visual mask mask (0 to 255), easier for humans to see.
                        image_mask_final.save(
                            os.path.join(
                                self.path_mask_visual, classID, filename_extension
                            )
                        )
                        # Save the crop to segmentation folders.
                        img_crop.save(
                            os.path.join(
                                self.path_segmentation, classID, filename_extension
                            )
                        )
                        # This saves the overlay,
                        img_overlay.save(
                            os.path.join(self.path_overlay, classID, filename_extension)
                        )
                    else:
                        # Save mask (0 to 1)
                        img_mask.save(os.path.join(self.path_mask, filename_extension))
                        # Save visual mask mask (0 to 255), easier for humans to see.
                        image_mask_final.save(
                            os.path.join(self.path_mask_visual, filename_extension)
                        )
                        # Save the crop to segmentation folders.
                        img_crop.save(
                            os.path.join(self.path_segmentation, filename_extension)
                        )
                        # This saves the overlay,
                        img_overlay.save(
                            os.path.join(self.path_overlay, filename_extension)
                        )

                    print(x_coords)
                    print(y_coords)
                    print(item_id)
                    print(norm_id)
                    print(product_name)
                    print(shelf_start, shelf_end)
                    # time.sleep(10000)

                except KeyboardInterrupt:
                    break
            reader.close()

    def csv_aggregation(self):
        list_csv = recursive_list_re(self.path_csv, ".csv")
        merged_csv = os.path.join(self.path_csv, "AggregatedInformation.csv")
        for file in list_csv:
            df = pd.read_csv(file)
            df.to_csv(merged_csv, index=False, mode="a", encoding="utf-8-sig")

    def create_class_folder(self, classID):
        """
        Create the class specific folder if they do not exist yet.
        :param classID:
        :return:
        """
        classID = str(classID)
        os.makedirs(self.path_csv.joinpath(classID), exist_ok=True)
        os.makedirs(self.path_mask.joinpath(classID), exist_ok=True)
        os.makedirs(self.path_mask_visual.joinpath(classID), exist_ok=True)
        os.makedirs(self.path_image.joinpath(classID), exist_ok=True)
        os.makedirs(self.path_segmentation.joinpath(classID), exist_ok=True)
        os.makedirs(self.path_overlay.joinpath(classID), exist_ok=True)


if __name__ == "__main__":
    test = visualizer()
    test.export_all()
    test.csv_aggregation()
