import pytest
from unified_log import logger
from preparation.prep_images.extract import generate_random_compatible_crop_size, crop_folder_bg


@pytest.mark.parametrize(
    "path_file",
    [
        r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\downloads\scene\5.4d09e09r3p501.jpg",
        r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\downloads\scene\1.31781-gettyimages-484274006.jpg",
        r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\downloads\scene\2.kitchen-scene.jpg",
        r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\downloads\scene\4.7tnc8g7f7nr41.jpg",
    ]
)
def test_generate_compatible_crop_size(path_file):
    logger.info(generate_random_compatible_crop_size(path_file))


def test_crop_folder_bg():
    crop_folder_bg(r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\downloads",
                   r"C:\yang\Dropbox\My PC (Envy)\Downloads\TestDownloads\Cropped",
                   100)
