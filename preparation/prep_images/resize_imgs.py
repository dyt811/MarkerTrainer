from PythonUtils.PUFolder import recursive_list
from PIL import Image, ImageOps
import os


class ImageProcessor:
    """
    A class of objects related to basic image processing. 
    """

    def __init__(self, img_size):
        """

        :param img_size: desire image size which will be fit to.  
        """
        self.img_size = img_size

    def resize_imgs(self, path):
        """
        Resize all images from the path, RECURSIVELY via BEST DIMENSIONAL FIT to the desired image size.
        :param path:
        :return:
        """
        for i, img_path in enumerate(recursive_list(path)):

            # Keep user posted.
            print(i, img_path)

            # Open data.
            img = Image.open(img_path)

            # Fit the data with required dimension WITH ANTIALIASING enabled.
            img = ImageOps.fit(
                img, (self.img_size[0], self.img_size[1]), Image.ANTIALIAS
            )

            # Keep the last part of the file name PRE .
            name = img_path.split("/")[-1].split(".")[0]

            # Create folder if not exist.
            if not os.path.exists(f"resized_{self.img_size[0]}_{self.img_size[1]}"):
                os.makedirs(f"resized_{self.img_size[0]}_{self.img_size[1]}")
            # Save to the folder while maintaining the name.
            img.save(f"resized_{self.img_size[0]}_{self.img_size[1]}/{name}.png")


if __name__ == "__main__":
    imgprocessor = ImageProcessor((128, 128))
    imgprocessor.resize_imgs(r"/data/resized_128_128")
