import os
from PIL import Image
import random
from tqdm import tqdm
from PythonUtils.PUFile import unique_name
from PythonUtils.PUFolder import recursive_list
from pathlib import Path
from labels.bbox import boundingbox
import logging
import sys
from typing import List

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


def overlay_image_randomly_save_output(bg_image_path: Path or str,
                                       overlay_images_paths: List[Path] or List[str],
                                       output_image_path: Path or str,
                                       overlay_iterations: int = 1,
                                       size_ratio_variation: float = 0.25):
    """
    Over lay an image on top of the background image, and output it to certain location
    This is the foundation function behind which various wrapper calls.

    Core function of the MotionCorrect augmentation process.

    :param size_ratio_variation:
    :param overlay_iterations:
    :param overlay_images_paths:
    :param bg_image_path:
    :param output_image_path:
    :return:
    """

    # suppose y_true and y_predict are your two images

    bg = Image.open(bg_image_path)

    bg_width, bg_height = bg.size
    list_groundtruth_boundingbox = []

    for overlay_image_path in overlay_images_paths:

        # Get overlay size
        overlay = Image.open(overlay_image_path)
        overlay_width, overlay_height = overlay.size

        for _ in range(overlay_iterations):

            # Scale Factor:
            scale_factor = 1 + size_ratio_variation * random.random()

            # Calculated the randomly scaled variants
            overlay_width_scaled: int = int(overlay_width * scale_factor)
            print(overlay_width_scaled - overlay_width)
            overlay_height_scaled: int = int(overlay_height * scale_factor)

            # Creat the smaller version of the overlay.
            resized_overlay = overlay.resize((overlay_width_scaled, overlay_height_scaled), Image.ANTIALIAS)

            if overlay_width_scaled > bg_width or overlay_height_scaled > bg_height:
                logger.info(
                    "Overlay is bigger than background, skipping! Check your foreground dimension vs bg dimension!"
                )
                continue

            # Coordinate calculation, try to keep the most marker visible BY using the old dimension before scaling
            # Occlusion is automatically taken care of when multiple marker overlap
            paste_width_range = bg_width - overlay_width
            paste_height_boundary = bg_height - overlay_height

            random_paste_x = int(random.uniform(0, paste_width_range))
            random_paste_y = int(random.uniform(0, paste_height_boundary))

            # Data Fields for the .txt output
            CATEGORY = 0
            X = (random_paste_x + overlay_width_scaled/2) / bg_width
            Y = (random_paste_y + overlay_height_scaled/2) / bg_height
            WIDTH = overlay_width_scaled / bg_width
            HEIGHT = overlay_height_scaled / bg_height

            list_groundtruth_boundingbox.append(boundingbox(CATEGORY, X, Y, WIDTH, HEIGHT))

            # Paste the overlay on top of the background image, using alpha channels as MASKs.
            bg.paste(resized_overlay, (random_paste_x, random_paste_y), resized_overlay)

        logger.info(f"Overlaid image {overlay_image_path} on {bg_image_path}. Saved to: {output_image_path}")
    bg.save(output_image_path, "PNG")

    # Swap out the image path with .txt
    path_label = Path(output_image_path).with_suffix(".txt")

    # {CATEGORY} {X} {Y} {WIDTH} {HEIGHT}
    with open(path_label, "w") as writer:
        for bbox in list_groundtruth_boundingbox:
            writer.write(f"{bbox}\n")


def overlay_folder_randomly(path_bg_folder, path_overlay_folder, output_root_path, samples=100):
    """
    Wrapper function that output to a UNIQUE subfolder instead of directly into the folder. For output into a SPECIFIC
    output folder, use the folder_foreground instead.
    :param path_bg_folder:
    :param path_overlay_folder:
    :param output_root_path:
    :return:
    """

    # Generate and make the directory
    overlaid_output_unique_path = os.path.join(output_root_path, "Overlay_" + unique_name())
    os.makedirs(overlaid_output_unique_path)

    # Get list of background.
    bg_list = recursive_list(path_bg_folder)

    # Get list of foreground.
    overlay_list = recursive_list(path_overlay_folder)

    # Overlist one on top of the other.
    overlay_list_randomly(bg_list, overlay_list, overlaid_output_unique_path, samples)

    return overlaid_output_unique_path


def overlay_subfolder(path_bg_folder, path_overlay_folder, output_root_path):
    """
    Wrapper function that output to a UNIQUE subfolder instead of directly into the folder. For output into a SPECIFIC
    output folder, use the folder_foreground instead.
    :param path_bg_folder:
    :param path_overlay_folder:
    :param output_root_path:
    :return:
    """
    # Generate and make the directory
    unique_path = os.path.join(output_root_path, "Overlay_" + unique_name())
    os.makedirs(unique_path)
    overlay_folder(path_bg_folder, path_overlay_folder, unique_path)

    return unique_path


def overlay_list_randomly(list_bg_path: list,
                          list_overlay_path: list,
                          output_path: Path or str,
                          n_samples_output: int,
                          n_samples_overlay: int = 3):
    """
    Output the combination of BG and OVERLAY into a TARGET folder. in random draw manner.
    :param bg_folder:
    :param overlay_folder:
    :param output_path:
    :return:
    """
    from random import randint

    # Generate this number of n_samples_output.
    output_sample_count = 0
    pbar = tqdm(n_samples_output)
    while output_sample_count < n_samples_output:
        # Randomly draw a bg
        bg_image_path = list_bg_path[randint(0, len(list_bg_path)) - 1]

        list_overlay_image_path = []
        # Randomly draw number of overlay
        for _ in range(n_samples_overlay):
            list_overlay_image_path.append(list_overlay_path[randint(1, len(list_overlay_path))-1])

        # Skip if size is too different.
        bg = Image.open(bg_image_path)

        list_filtered_overlay = []
        # Filter the list using lambda function to skip overlays that are bigger than the background size.
        list_filtered_overlay = list(filter(
            lambda single_image_path:
            Image.open(single_image_path).size[0] < bg.size[0] and Image.open(single_image_path).size[1] < bg.size[1],
            list_overlay_image_path
        ))

        # Attempt overlay.
        try:
            # Generate new image name.
            merged_image = os.path.join(output_path, unique_name() + ".png")
            overlay_image_randomly_save_output(bg_image_path, list_filtered_overlay, merged_image)

            output_sample_count = output_sample_count + 1  # only increase counter if successfully generated one
            pbar.update(output_sample_count)
        except FileNotFoundError or OSError or IOError:
            logger.info("Bad image found during overlay: " + bg_image_path)
            continue

    pbar.close()


def overlay_folder(path_bg_folder, path_overlay_folder, output_path):
    """
    Output the combinatino of BG and OVERLAY into a TARGET folder after all possible combination.
    :param path_bg_folder:
    :param path_overlay_folder:
    :param output_path:
    :return:
    """
    from PythonUtils.PUFolder import recursive_list

    bg_list = recursive_list(path_bg_folder)
    overlay_list = recursive_list(path_overlay_folder)

    for bg_image in tqdm(bg_list):
        for overlay_image in overlay_list:
            try:
                # Generate new image name.
                merged_image = os.path.join(output_path, unique_name() + ".png")
                overlay_image_randomly_save_output(bg_image, overlay_image, merged_image)
            except FileNotFoundError:
                logger.info(bg_image)
                continue


if __name__ == "__main__":
    overlay_image_randomly_save_output("../a.jpg", "../b.JPG", 0.1, "new.jpg")
