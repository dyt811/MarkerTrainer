from PIL import Image
import random
import os
from PythonUtils.PUFile import unique_name, duplicates_into_folders
from PythonUtils.PUFolder import recursive_list
import logging
import sys
from tqdm import tqdm
import shutil
from typing import Tuple

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)
"""
This class explores the images and crop out as many smaller conforming image section as possible. 
"""

common_3t2_resolution_list = [
    (480, 320),
    (1152, 768),
    (1280, 854),
    (1440, 960)
]
common_4t3_resolution_list = [
    (320, 240),
    (384, 288),
    (640, 460),
    (768, 576),
    (800, 600),
    (1024, 768),
    (1152, 864),
    (1280, 960),
    (1400, 1050),
    (1440, 1080),
    (1600, 1200),
    (2048, 1536)
]

common_5t3_resolution_list = [
    (800, 480),
    (1280, 768),
]

common_5t4_resolution_list = [
    (1280, 1024),
]

common_8t5_resolution_list = [
    (320, 200),
    (1280, 800),
    (1440, 900),
    (1680, 1050),
    (1920, 1200),
]

common_16t9_resolution_list = [
    (854, 480),
    (1024, 576),
    (1280, 720),
    (1366, 768),
    (1600, 900),
    (1920, 1080)
]

common_17t9_resolution_list = [
    (2048, 1080)
]

all_resolution_list = common_17t9_resolution_list + \
                      common_16t9_resolution_list + \
                      common_8t5_resolution_list + \
                      common_5t4_resolution_list + \
                      common_3t2_resolution_list + \
                      common_5t3_resolution_list + \
                      common_4t3_resolution_list

all_resolution_list.sort()

def crop_area(image, x, y, width, height):
    """
    Crop and image using the given coordinate based on the width and height given.
    :param image:
    :param x:
    :param y:
    :param width:
    :param height:
    :return:
    """
    img = Image.open(image)
    area = (x, y, x + width, y + height)
    cropped_img = img.crop(area)
    # cropped_img.show()
    return cropped_img


def crop_randomly(image, width, height):
    """
    Randomly crop areas of the image that is equivalent to the given width and height.
    :param image:
    :param width:
    :param height:
    :return:
    """
    img = Image.open(image)
    img_width, img_height = img.size

    crop_wide_boundary = img_width - width
    crop_height_boundary = img_height - height
    if crop_height_boundary < 0 or crop_wide_boundary < 0:
        return None
    random_crop_x = int(random.uniform(0, crop_wide_boundary))
    random_crop_y = int(random.uniform(0, crop_height_boundary))

    return crop_area(image, random_crop_x, random_crop_y, width, height)


def generate_random_compatible_crop_size(image_path) -> Tuple[int, int]:
    """
    Given an image, randomly produce a viable commonly used resolution (or switched resolution) for extraction size:
    width, height
    :param image_path:
    :return: Tuple of Width and Height
    """

    img = Image.open(image_path)
    img_width, img_height = img.size

    # Filter to select the resolution that is compatible.
    viable_resolution_list = list(filter(
        lambda resolution_size: resolution_size[0] <= img_width and resolution_size[1] <= img_height, all_resolution_list
    ))
    viable_switched_resolution_list = list(filter(
        lambda resolution_size: resolution_size[0] <= img_height and resolution_size[1] <= img_width, all_resolution_list
    ))

    # Randint can be max, so has to be 1 based instead of 0 based.
    chosen_dimension_resolution = random.randint(1, len(viable_resolution_list)+len(viable_switched_resolution_list)) - 1

    # try:
    if chosen_dimension_resolution >= len(viable_resolution_list):
        chosen_switch_dimension_resolution = chosen_dimension_resolution - len(viable_resolution_list)
        (width, height) = viable_switched_resolution_list[chosen_switch_dimension_resolution]
    else:
        (height, width) = viable_resolution_list[chosen_dimension_resolution]
    # except IndexError:
    #     logger.debug("Chosen at: {chosen_dimension_resolution}")
    #     logger.debug(viable_resolution_list)
    #     logger.debug("Pause here!")

    return width, height


def crop_filelist(filelist, output_folder, iterations):
    """
    crop the background images and generate the cropped version of them that are only 500x500
    :param iterations:
    :param output_folder:
    :param filelist:
    :return:
    """

    # Duplicate input file lists X iterations into the output destination
    updated_filelist = duplicates_into_folders(filelist, output_folder, iterations)

    # For all the files, try to convert and export to that address
    for file in tqdm(updated_filelist):
        try:
            image_path = file

            # Call a function to randomly determined compatible common image size (Width Height could be switched
            width, height = generate_random_compatible_crop_size(image_path)

            image = crop_randomly(image_path, width, height)
        except OSError:
            logger.info("Found a bad file. Ignoring: " + file)
            continue

        bg_cropped_path = os.path.join(output_folder, unique_name() + ".png")

        if image is None:
            continue
        else:
            try:
                # Generate a RGB image from the cropped image. This FORCE the image to be RGB even if it was originally GRAY scale!
                rgbimg = Image.new("RGBA", image.size)

                # Paste the image in.
                rgbimg.paste(image)

                # Save the file.
                rgbimg.save(bg_cropped_path, "PNG")
                logger.info("Saved " + bg_cropped_path)

                # Delete the original
                os.remove(file)
            except OSError:
                logger.info(
                    "Found a bad file. Ignoring: " + file + " from " + image_path
                )
                continue


def crop_folder(image_folder, output_folder, iterations):
    """
    crop the background images and generate the cropped version of them
    :param iterations:
    :param image_folder: folder contain downloads.
    :param output_folder: folder where all the output will be dumped into
    :return:
    """
    # Change into the directory
    # List all the relevant folders.
    files = recursive_list(image_folder)

    # Make DIR if it does not already exist.
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    crop_filelist(files, output_folder, iterations)


def crop_folder_bg(
        input_image_root_folder, output_root_folder, iterations=5
):
    """
    A batch function for cropping the background images and generate the cropped version of them that are only 500x500
    :param iterations:
    :param output_root_folder:
    :param input_image_root_folder: folder contain downloads.
    :return:
    """
    # Generate the temp name that will be used to store the crop results.
    crop_folder_name = f"cropped_images_{unique_name()}"

    # Combine folder and root to form path name.
    output_subfolder = os.path.join(output_root_folder, crop_folder_name)

    # Make DIR if it does not already exist.
    if not os.path.exists(output_subfolder):
        os.makedirs(output_subfolder)

    crop_folder(input_image_root_folder, output_subfolder, iterations)

    return output_subfolder


if __name__ == "__main__":
    # randomly("../a.jpg", 400, 400)
    crop_folder_bg(r"E:\Gitlab\MarkerTrainer\bg_data\background\\", 400, 400)
