from preparation.prep_images.overlay import overlay_image_randomly_save_output, overlay_folder_randomly, overlay_list_randomly


def test_overlay_image_randomly_save_output():
    overlay_image_randomly_save_output(r"C:\yang\Dropbox\My PC (Envy)\Downloads\cute-cat-videos-lede.jpg",
                                       r"C:\git\MarkerTrainer\aug\foreground\Prime\100.png",
                                       r"C:\yang\Dropbox\My PC (Envy)\Downloads\Combined.png",
                                       overlay_iterations=5,
                                       size_ratio_variation=5)

def test_overlay_folder():
    overlay_folder_randomly(r"B:\TrainingData\augmented_bg",
                   r"B:\TrainingData\augmented_marker\2021-01-10T21_26_42.066097Aug100ImgAug",
                   r"D:\CombinedMarker",
                            samples=490000
    )