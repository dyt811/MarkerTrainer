from PIL import Image
import random
import os
from PythonUtils.PUFile import unique_name, duplicates_into_folders
from PythonUtils.PUFolder import recursive_list_re
import logging
import sys
from tqdm import tqdm
import shutil
from pathlib import Path
import numpy as np
from typing import List


def binary_to_value(
    list_path_files: List[str] or List[Path],
    path_output: Path,
    image_values: List[int] or int = 255,
):
    """
    Conver a binary image to gray scale values using a list of a single int
    :param list_path_files:
    :param image_values:
    :return:
    """
    for index, file in enumerate(list_path_files):
        data = Image.open(file)
        image_binary = np.array(data)
        if type(image_values) is int:  # the default path to paint the area 255
            image_grayscale = image_binary * image_values
        else:
            assert len(list_path_files) == len(image_values)
            image_grayscale = image_binary * image_values[enumerate]
        image_converted = Image.fromarray(image_grayscale)
        path_converted_file = path_output / os.path.basename(file)
        image_converted.save(path_converted_file)
    print("Conversion finished")


if __name__ == "__main__":
    path_black = r"C:\Users\Yang Ding\Desktop\Stockwell\model_test_folder\Test_Automated\model_v2\2019-09-24T11_05_31.300978_FinalModelWeights_model.Kaggle_DeepLabV3Plus.ModelClassSpec.h5"
    images = recursive_list_re(path_black, "Output")
    binary_to_value(images, Path(path_black))
