from pathlib import Path
from random import random
import multiprocessing
from joblib import Parallel, delayed
from tqdm import tqdm

path_augmented_image_labels: Path = Path(r"D:\CombinedMarker\Overlay_2021-01-26T08_59_02.551812")

import glob

import time

# 4 seconds to list.
start = time.time()
list_images = glob.glob(str(path_augmented_image_labels / "*.png"))
end = time.time()
print(len(list_images))
print(end - start)
start = time.time()

# 4 seconds to list.
list_txts = glob.glob(str(path_augmented_image_labels / "*.txt"))
end = time.time()
print(len(list_txts))
print(end - start)

import os
path_images_train = Path(r"D:\CombinedMarker\images\train")
path_images_val = Path(r"D:\CombinedMarker\images\val")
path_images_test = Path(r"D:\CombinedMarker\images\test")

path_labels_train = Path(r"D:\CombinedMarker\labels\train")
path_labels_val = Path(r"D:\CombinedMarker\labels\val")
path_labels_test = Path(r"D:\CombinedMarker\labels\test")


os.mkdir(str(path_images_train))
os.mkdir(str(path_images_val))
os.mkdir(str(path_images_test))

os.mkdir(str(path_labels_train))
os.mkdir(str(path_labels_val))
os.mkdir(str(path_labels_test))


num_cores = multiprocessing.cpu_count()
inputs = tqdm(list_images)

def sort_into_train_val(path_image_str):
    lottery = random()
    path_image_object = Path(path_image_str)
    path_label_object = Path(path_image_str).with_suffix(".txt")
    path_label_object_str = str(path_label_object)
    if lottery < 0.5:
        os.rename(path_image_str, str(path_images_train / path_image_object.name))
        os.rename(path_label_object_str, str(path_labels_train / path_label_object.name))
    elif lottery < 0.75:
        os.rename(path_image_str, str(path_images_val / path_image_object.name))
        os.rename(path_label_object_str, str(path_labels_val / path_label_object.name))
    elif lottery <= 1:
        os.rename(path_image_str, str(path_images_test / path_image_object.name))
        os.rename(path_label_object_str, str(path_labels_test / path_label_object.name))


if __name__ == "__main__":
    processed_list = Parallel(n_jobs=num_cores)(delayed(sort_into_train_val)(path_image) for path_image in inputs)
