import idx2numpy
import cv2
import numpy as np
from PIL import Image
from PythonUtils.PUFile import unique_name
from PythonUtils.PUFolder import create_batch, create
from pathlib import Path
import urllib.request
import gzip
import shutil
import os
import matplotlib.pyplot as plt
from tqdm import tqdm

path_data = Path(r"../data_mnist")
print(str(path_data.absolute()))
files = [
    "train-images-idx3-ubyte",
    "train-labels-idx1-ubyte",
    "t10k-images-idx3-ubyte",
    "t10k-labels-idx1-ubyte",
]
gz_extension = ".gz"


def initialize_raw_data_mnist_folder(output_path_data: Path or str):
    """
    !!!WILL DELETE EVERYHING IN /data_mnist if such folder already exist, NO QUESTION ASKED!!!
    Download from the web site and setup the data directory as needed.
    :return:
    """
    # Force Path type.
    output_path_data = Path(output_path_data)

    create(output_path_data.absolute(), nuke_it=True, really_nuke_it=True)
    assert output_path_data.exists()

    try:
        for file in files:

            # Download
            path_file = output_path_data / (file + gz_extension)
            urllib.request.urlretrieve(
                r"http://yann.lecun.com/exdb/mnist/" + file + gz_extension, path_file
            )

            # Gzip decompress
            with gzip.open(path_file, "rb") as f_in:
                with open(os.path.splitext(str(path_file))[0], "wb") as f_out:
                    shutil.copyfileobj(f_in, f_out)

            # Delete the original GZ file.
            os.remove(str(path_file))
    except Exception as e:
        print(e)
        shutil.rmtree(output_path_data)


def prepare_mnist_data(list_input_path: Path or str, output_path_data: Path or str):
    """
    Decompress the data files into actual images to do visual quality check.
    :return:
    """
    # Force Path type.
    list_input_path = Path(list_input_path)
    output_path_data = Path(output_path_data)

    # Specify the output folders.
    path_train = output_path_data.absolute() / "train"
    path_label = output_path_data.absolute() / "label"
    path_holdout_train = output_path_data.absolute() / "holdout_train"
    path_holdout_label = output_path_data.absolute() / "holdout_label"

    image_extension = ".jpeg"

    try:
        create_batch([path_train, path_label, path_holdout_train, path_holdout_label])

        arrary_train_images = idx2numpy.convert_from_file(
            str(list_input_path / files[0])
        )
        arrary_train_label = idx2numpy.convert_from_file(
            str(list_input_path / files[1])
        )
        arrary_test_images = idx2numpy.convert_from_file(
            str(list_input_path / files[2])
        )
        arrary_test_label = idx2numpy.convert_from_file(str(list_input_path / files[3]))

        # arr is now a np.ndarray type of object of shape 60000, 28, 28
        for index, image in tqdm(enumerate(arrary_train_images)):
            PIL_image = Image.fromarray(image)
            PIL_image.save(path_train / (f"{index:05d}" + image_extension))
            # plt.imsave(path_train / (unique_name() + image_extension), image)

        np.savetxt(path_label / "label.csv", arrary_train_label, delimiter=",")

        for index, image in tqdm(enumerate(arrary_test_images)):
            PIL_image = Image.fromarray(image)
            PIL_image.save(path_holdout_train / (f"{index:05d}" + image_extension))
            # plt.imsave(path_holdout_train / (unique_name() + image_extension), image)

        np.savetxt(path_holdout_label / "label.csv", arrary_test_label, delimiter=",")
    except Exception as e:
        # Incase the operation failed, remove the entire output folder.
        print(e)
        shutil.rmtree(output_path_data)


if __name__ == "__main__":
    initialize_raw_data_mnist_folder(path_data)
    # prepare_mnist_data()
