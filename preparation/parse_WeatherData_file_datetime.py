import re
import os
from typing import List
from datetime import datetime
from pathlib import Path

regex_train_file_name = re.compile(r"training_\d\d\d\d_\d\d\d\d.npy")
regex_validation_file_name = re.compile(r"validation_\d\d\d\d_\d\d\d\d.npy")
regex_datetime = re.compile(r"201\d-\d\d-\d\d \d\d:\d\d:\d\d")


def parse_spec_file(path_data: str) -> (List[str], List[datetime]):
    """
    Specialize function to used to read date.txt, and help split out the file names and the datetimepoints
    :param path_data:
    :return:
    """
    # read the csv file with pandas
    # self.df = pd.read_csv(path_data_root)
    path_folder = os.path.dirname(Path(path_data))

    file = open(Path(path_data), "r")
    file_lines = file.readlines()
    file.close()
    list_filename = []
    list_datetime = []

    for file in file_lines:
        success, file_name, datetime_start, datetime_end = parse_spec_row(file)
        if success:
            list_filename.append(file_name)
            list_datetime.append(datetime_start)

    assert len(list_datetime) == len(list_filename)

    return list_filename, list_datetime


def parse_spec_row(row_string: str) -> (bool, str, List[str]):
    """
    This process one row of such type of file specification
    :param row_string:
    :return:
    """
    default_invalid_return = (False, None, None, None)

    if row_string == "\n":
        return default_invalid_return

    if not ".npy" in row_string:
        return default_invalid_return
    try:
        filename = regex_train_file_name.search(row_string)
        filename1 = regex_validation_file_name.search(row_string)
        date = regex_datetime.findall(row_string)

        if filename is None and filename1 is not None and date is not None:
            return True, filename1.group(), date[0], None
        elif filename is not None and filename1 is None and date is not None:
            return True, filename.group(), date[0], date[1]
        else:
            return default_invalid_return
    except:
        return default_invalid_return


if __name__ == "__main__":
    # a, b = parse_spec_file(r"C:\Temp\WeatherChallenge\data\date.txt")
    # a, b = parse_spec_file(r"C:\Temp\WeatherChallenge\data\dataset_train - Copy.txt")
    # a, b = parse_spec_file(r"C:\Temp\WeatherChallenge\data\dataset_train.txt")
    a, b = parse_spec_file(r"E:\WeatherData\data\dataset_validate.txt")

    pass
