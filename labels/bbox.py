from dataclasses import dataclass

@dataclass
class boundingbox:
    # Represent a bbox which was created to bound the

    category: int
    x: float
    y: float
    width: float
    height: float

    def __repr__(self):
        return f"{self.category} {self.x} {self.y} {self.width} {self.height}"


if __name__=="__main__":
    a = boundingbox(1, 0.1, 0.2, 0.3, 0.4)
    print(a)