from tqdm import tqdm
from enum import Enum


"""
This is the batch function which runs the TEMPORAL model which has, ironically, removed the temporal component. 
"""


class ModelType(Enum):
    ANN = 0
    CNN = 1


from evaluation.batch_function import visualize, score
from evaluation.model_load_predict import (
    run_ANN_model_on_data,
    run_CNN_model_on_data,
    run_CNN_temporal_model_on_data,
)
from preparation.parse_WeatherData_file_datetime import parse_spec_file
from model.load import load_model
from PythonUtils.PUFolder import create
from PythonUtils.PUFile import unique_name


def batch_call(
    list_truth: list,
    list_prediction: list,
    list_air_temp: list,
    list_dew_temp: list,
    list_timepoint: list,
    function_name,
):
    """
    Carry out batch functions on per item from the above list.
    :param list_truth:
    :param list_prediction:
    :param list_air_temp:
    :param list_dew_temp:
    :param list_timepoint:
    :param function_name: the function to be called, usually save, calculate score etc.
    :return:
    """
    # They all should have the same length to ensure indexing efficiency.
    assert (
        len(list_truth)
        == len(list_prediction)
        == len(list_air_temp)
        == len(list_dew_temp)
        == len(list_timepoint)
    )

    list_output = []

    for temporal_index in range(len(list_truth)):
        list_output.append(
            function_name(
                list_air_temp[temporal_index],
                list_dew_temp[temporal_index],
                list_prediction[temporal_index],
                list_truth[temporal_index],
                list_timepoint[temporal_index].isoformat(),
            )
        )
    return list_output


def run_batch(
    path_eval_files=r"E:\WeatherData\data\dataset_validate.txt",
    path_model=r"F:\Git\MarkerTrainer\models\2019-06-22T11_03_31.593789_model.I720896_O65536_CNN.HighDimensionalTo2D",
):
    import os

    folder = os.path.dirname(path_eval_files)

    # wipe score file if already exist.
    score_file = os.path.join(folder, "score.csv")
    if os.path.isfile(score_file):
        os.remove(score_file)

    files, datetimes = parse_spec_file(path_eval_files)

    os.chdir(folder)
    result_folder = f"{unique_name()}_evaluation_{os.path.basename(path_model)}"
    create(result_folder)

    loaded_model = load_model(path_model)

    # loop through all the files from the path specification
    for index in tqdm(range(len(files))):
        os.chdir(folder)

        list_truth = (
            list_prediction
        ) = list_air_temp = list_dew_temp = list_timepoint = []

        file_name = files[index]
        particular_datetime = datetimes[index]

        if "I19" in path_model:
            model_type = ModelType.ANN
            list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint = run_ANN_model_on_data(
                loaded_model, file_name, particular_datetime
            )
        elif "I983040" in path_model:
            model_type = ModelType.CNN
            list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint = run_CNN_model_on_data(
                loaded_model, file_name, particular_datetime
            )
        elif "718302.h5" in path_model or "I720896_O65536" in path_model:
            model_type = ModelType.CNN
            list_truth, list_prediction, list_air_temp, list_dew_temp, list_timepoint = run_CNN_temporal_model_on_data(
                loaded_model, file_name, particular_datetime
            )

        os.chdir(result_folder)

        batch_call(
            list_truth,
            list_prediction,
            list_air_temp,
            list_dew_temp,
            list_timepoint,
            visualize,
        )
        batch_call(
            list_truth,
            list_prediction,
            list_air_temp,
            list_dew_temp,
            list_timepoint,
            score,
        )


if __name__ == "__main__":
    run_batch(
        path_eval_files=r"E:\WeatherData\data\dataset_validate.txt",
        path_model=r"F:\Git\MarkerTrainer\models\2019-06-22T11_03_31.593789_model.I720896_O65536_CNN.HighDimensionalTo2D",
    )
