import os
import numpy as np
from matplotlib import pyplot as plt

from evaluation.scoreFunction import getScore


def expand2(array: np.ndarray):
    """
    Pad the array with 1 on two axis for TF data loading.
    :param array:
    :return:
    """
    a = np.expand_dims(array, 0)
    b = np.expand_dims(a, 0)
    return b


def score_caller(input1, input2):
    """
    A cute wrapper of the score functiont o automate matrix expansion and maximum calculation process.
    :param input1:
    :param input2:
    :return:
    """
    maximum_input1 = np.ndarray.max(input1)
    maximum_input2 = np.ndarray.max(input2)

    score = getScore(
        expand2(input1), expand2(input2), max(maximum_input1, maximum_input2)
    )

    return score


def score(
    air_temperature: np.ndarray,
    dew_temperature: np.ndarray,
    prediction: np.ndarray,
    ground_truth: np.ndarray,
    isodate: str,
):
    """
    Quad panel temperature visualization of RAW data along with prediction.
    :param air_temperature:
    :param dew_temperature:
    :param prediction:
    :param ground_truth:
    :param isodate:
    :return:
    """

    score_air = score_caller(air_temperature, ground_truth)
    score_dew = score_caller(dew_temperature, ground_truth)
    score_prediction = score_caller(prediction, ground_truth)

    if os.path.exists("score.csv"):
        append_write = "a"  # append if already exists
    else:
        append_write = "w"  # make a new file if not

    highscore = open("score.csv", append_write)
    highscore.write(f"{score_air[0]}, {score_dew[0]}, {score_prediction[0]}\n")
    highscore.close()

    # always in the order of air, dew, prediction from now on.
    return score_air, score_dew, score_prediction, isodate


def visualize(
    air_temperature: np.ndarray,
    dew_temperature: np.ndarray,
    prediction: np.ndarray,
    ground_truth: np.ndarray,
    isodate: str,
):
    """
    Quad panel temperature visualization of RAW data along with prediction.
    :param air_temperature:
    :param dew_temperature:
    :param prediction:
    :param ground_truth:
    :param isodate:
    :return:
    """
    score_air, score_dew, score_prediction, isodate = score(
        air_temperature, dew_temperature, prediction, ground_truth, isodate
    )

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 10))

    # 5 is the index of the temperature among 15 variables.

    ax1.imshow(air_temperature)
    ax1.set_title(f"Air temperature:\n{isodate}\n{score_air}")

    ax2.imshow(dew_temperature)
    ax2.set_title(f"Dew temperature:\n{isodate}\n{score_dew}")

    ax3.imshow(prediction)
    ax3.set_title(f"Prediction temperature:\n{isodate}\n{score_prediction}")

    ax4.imshow(ground_truth)
    ax4.set_title(f"Ground truth temperature:\n{isodate}")

    plt.figure()
    # plt.show()
    fig.savefig(f'Comparison_{isodate.replace(":","")}.png')
