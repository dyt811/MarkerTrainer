# The purpose of this class is to render the images into MOVIES such that you can see.
from pathlib import Path
import matplotlib.animation as animation
from pylab import *
import glob
from tqdm import tqdm
from evaluation.visualize import visualize_save
from PythonUtils.PUFolder import create
from matplotlib import pyplot
import gc

dpi = 100
# Current data file index.
data_file_index = 0
import os


def png2mov(png_path):
    """
    Python rendering movies from the png
    :param png_path:
    :return:
    """
    import cv2

    image_folder = "images"
    video_name = "video.avi"

    images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape

    video = cv2.VideoWriter(video_name, 0, 1, (width, height))

    for image in images:
        video.write(cv2.imread(os.path.join(image_folder, image)))

    cv2.destroyAllWindows()
    video.release()


def call_ffmpeg(folder_jpeg):
    """
    External ffmpeg call to render the movies from a series of PNGs
    :return:
    """
    path_jpeg = Path(folder_jpeg)
    path_movie = path_jpeg.as_posix() + "_movie.mp4"

    os.system(
        r"magick convert E:\WeatherData\data\stamped\Var0\*.png  E:\WeatherData\data\stamped\Var0\movie.mp4"
    )


def render_movie_direct(data_path, index_variable: int):
    """
    Directly render the movie IN MEMORY. Not very time efficient although SPACE efficient. For > 1000 data points, use indirect renderings.
    :param data_path:
    :param index_variable:
    :return:
    """

    path_data = Path(data_path)
    list_files_input = glob.glob((path_data / "input_*").as_posix())

    # Create figure object.
    plt_figure = plt.figure()

    # Set plot to 1 x 1 x 1, which is only a SINGLE plot.
    plt_ax = plt_figure.add_subplot(111)

    # Set same aspect ration.
    plt_ax.set_aspect("equal")

    # Hide both axes.
    plt_ax.get_xaxis().set_visible(False)
    plt_ax.get_yaxis().set_visible(False)

    # Show the image of a RANDOM noise.
    plt_image = plt_ax.imshow(
        rand(256, 256),
        # cmap='gray',
        interpolation="nearest",
    )
    plt_image.set_clim([0, 1])
    plt_figure.set_size_inches([10, 10])
    tight_layout()

    # Total data files count.
    data_file_count = len(list_files_input)
    # data_file_count = 30

    # The progress bar to display where we are current at.
    pbar = tqdm(total=data_file_count)

    def update_image(n):
        """
        An inner function that updates the data of the plt_image.
        :param n:
        :return:
        """
        global data_file_index  # use the variable from one scope out

        # Load File:
        matrix3d_raw = np.load(list_files_input[data_file_index])

        # Increment the file index by one.
        data_file_index = data_file_index + 1

        # Load the slice of it should be: 15 x 256 x 256
        matrix2d_single_variable = matrix3d_raw[index_variable, :, :]

        # Update the data from the 2D Matrix.
        plt_image_data = matrix2d_single_variable

        # Update PLT IMAGE
        plt_image.set_data(plt_image_data)

        txt = list_files_input[data_file_index]
        plt_figure.text(
            0.5, 0.001, txt, wrap=True, horizontalalignment="center", fontsize=12
        )

        # Update the progress bar by one image loaded.
        pbar.update(1)

        return plt_image

    # legend(loc=0)

    # The animation main routine.
    plt_animation = animation.FuncAnimation(
        plt_figure, update_image, data_file_count, interval=30
    )

    # File writer.
    plt_writer = animation.writers["ffmpeg"](fps=30)

    # Save the file.
    plt_animation.save(
        f"WeatherDataChallengeVariableRendering_{index_variable}.mp4",
        writer=plt_writer,
        dpi=dpi,
    )

    # Close progress bar.
    pbar.close()
    return plt_animation


def render_movie_to_file(data_path, index_variable: int):
    """
    Directly render the movie IN MEMORY. Not very time efficient although SPACE efficient. For > 1000 data points, use indirect renderings.
    :param data_path:
    :param index_variable:
    :return:
    """

    path_data = Path(data_path)

    path_rendering = path_data / f"Var{index_variable}"
    create(path_rendering.as_posix())

    list_files_input = glob.glob((path_data / "input_*").as_posix())

    # The progress bar to display where we are current at.
    for file in tqdm(list_files_input):

        path_file = Path(file)

        # Load File:
        matrix3d_raw = np.load(file)

        # Load the slice of it should be: 15 x 256 x 256
        matrix2d_single_variable = matrix3d_raw[index_variable, :, :]

        fig = visualize_save(matrix2d_single_variable, path_file.name)

        fig.savefig(path_rendering / path_file.stem)
        fig.clf()
        plt.close()
        pyplot.close(fig)
        del matrix2d_single_variable
        del fig
        gc.collect()
        pyplot.close("all")


if __name__ == "__main__":

    # Loop through all 15 variables across the ENTIRE dataset and render them out in a movie.
    # index_var = 5
    for index_var in range(15):
        # render_movie_to_file("E:\WeatherData\data\stamped", index_var)
        call_ffmpeg(rf"E:\WeatherData\data\stamped\Var{index_var}")
