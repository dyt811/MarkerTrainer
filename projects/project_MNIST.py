# 2019-12-10T185527EST
# Example project used to validate the pipeline overall fitness


import luigi
from PythonUtils.PUFile import unique_name
from pathlib import Path

path_project = Path(r"../data_mnist")

# Data Download Phase
class DownloadExpData(luigi.Task):
    def run(self):
        # Import the main executable and pass the main path.
        from preparation.parse_i2_minist import initialize_raw_data_mnist_folder

        initialize_raw_data_mnist_folder(self.output().path)

    def output(self):
        return luigi.LocalTarget(path_project / "raw")


# Data Preparation Phase
class ParseExpData(luigi.Task):
    def requires(self):
        return (
            DownloadExpData()
        )  # the output is a Luigi LocalTarget, this will be equal to the self.input()

    def run(self):
        from preparation.parse_i2_minist import prepare_mnist_data

        prepare_mnist_data(self.input().path, self.output().path)

    def output(self):
        return luigi.LocalTarget(path_project / "parsed")


# Data Augmentation
class AugExpData(luigi.Task):
    def requires(self):
        return ParseExpData()

    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "augmented", unique_name())


# Running DeepLearning training
class TrainExpData(luigi.Task):
    def requires(self):
        return AugExpData()

    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "model", unique_name())


# Packaging for Distribution Stage


if __name__ == "__main__":
    luigi.build([ParseExpData()])
