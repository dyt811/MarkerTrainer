# 2019-11-11T190952EST
# Sourced from https://www.mlflow.org/docs/latest/tutorial.html
import luigi
from PythonUtils.PUFile import unique_name
from pathlib import Path

path_project = Path(r"C:\GitHub\MarkerTrainer\data_template")

# Data Download Phase
class DownloadExpData(luigi.Task):
    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "raw", unique_name())


# Data Preparation Phase
class ParseExpData(luigi.Task):
    def requires(self):
        return [DownloadExpData()]

    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "parsed", unique_name())


# Data Augmentation
class AugExpData(luigi.Task):
    def requires(self):
        return [DownloadExpData(), ParseExpData()]

    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "augmented", unique_name())


# Running DeepLearning training
class TrainExpData(luigi.Task):
    def requires(self):
        return [ParseExpData(), AugExpData()]

    def run(self):
        pass

    def output(self):
        return luigi.LocalTarget("path_project", "model", unique_name())


# Packaging for Distribution Stage
