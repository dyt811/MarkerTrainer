from keras.utils import Sequence
import random
import math
import os
from pathlib import Path
from PythonUtils.PUFolder import recursive_list_re
from PythonUtils.PUFile import unique_name
from PythonUtils.PUDateTime import tstr2iso
import numpy as np
import datetime
import logging
from generator.current_mode import ProcessingMode
from datetime import timedelta
import re
from preparation.parse_WeatherData_file_datetime import parse_spec_file

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger()


def decipher_isodatestr(file_name: str):
    """
    Try to get the timestamp datetime from the file name.
    :param file_name:
    :return:
    """
    regex_datetime = re.compile(r"201\d-\d\d-\d\dT\d\d\d\d\d\d")
    date = regex_datetime.findall(file_name)
    if date is not []:
        return date[0]
    else:
        return None


def date_split(current_date: datetime.datetime):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """

    new_year_day = datetime.datetime(
        year=current_date.year, month=1, day=1, hour=0, minute=0, second=0
    )

    date_differences = current_date - new_year_day
    day_of_the_year = date_differences.days + 1
    time_of_the_day = current_date.hour

    return day_of_the_year, time_of_the_day


def datetime_increment(
    input_date, number_of_three_hours_increment: int, format="%Y-%m-%d %H:%M:%S"
):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """

    data_row_date = input_date + number_of_three_hours_increment * timedelta(hours=3)

    return data_row_date


def datetime_str_increment(
    input_date: str, number_of_three_hours_increment: int, format="%Y-%m-%d %H:%M:%S"
):
    """
    Split the date into day count and hour count.
    :param current_date:
    :param format:
    :return:
    """
    current_date = datetime.datetime.strptime(input_date, format)

    data_row_date = current_date + number_of_three_hours_increment * timedelta(hours=3)

    return data_row_date


def flatten_input(matrix_input, file_start_date_time):
    """
    This function prepares the loaded datamatrix into the right format by ruthlessly crashing the temporal and spatial dimensions.
    :param matrix_input: 100 by 256 by 256 by 15
    :param file_start_date_time:
    :return matrix_time_inserted:6553600, 19
    """

    # matrix_transposed should have 256 by 256 by 1 by 15
    matrix_transposed = matrix_input.transpose([2, 3, 0, 1])
    matrix_transposed = matrix_input.transpose([2, 3, 0, 1])
    logger.info(matrix_transposed.shape)
    size_x_dimension = matrix_transposed.shape[
        0
    ]  # time point is the 3rd timepoint, 2 in zero based index.
    size_y_dimension = matrix_transposed.shape[
        1
    ]  # time point is the 3rd timepoint, 2 in zero based index.
    size_temporal_dimension = matrix_transposed.shape[
        2
    ]  # time point is the 3rd timepoint, 2 in zero based index.

    # Insert placeholder 0s for X positional information.
    matrix_x_inserted = np.insert(
        matrix_transposed, 15, values=0, axis=3
    )  # x coordinate.

    # Encoding the X position information into each cell:
    for index_x in range(size_x_dimension):
        # recall the TIME in consistent across the 65536 area:
        matrix_x_inserted[
            index_x, :, :, 15
        ] = index_x  # 15 = 16th position in zero based index.
    logger.info(matrix_x_inserted.shape)

    # insert placeholder 0s for Y positional information.
    matrix_y_inserted = np.insert(
        matrix_x_inserted, 16, values=0, axis=3
    )  # x coordinate.

    # Encoding the Y position information into each cell:
    for index_y in range(size_y_dimension):
        # recall the TIME in consistent across the 65536 area:
        matrix_y_inserted[
            :, index_y, :, 16
        ] = index_y  # 16 = 17th position in zero based index.
    logger.info(matrix_y_inserted.shape)

    # insert placeholder 0s for DAY information.
    matrix_time_inserted = np.insert(matrix_y_inserted, 17, values=0, axis=3)  # day row
    matrix_time_inserted = np.insert(
        matrix_time_inserted, 18, values=0, axis=3
    )  # time row

    # Encoding the temporal data into each cell:
    for index_time_increment in range(size_temporal_dimension):
        # Timedelta to calculate the offsetted date time.
        current_index_datetime = datetime_str_increment(
            file_start_date_time, index_time_increment
        )

        # Split that into HOUR and Day of the YEAR component.
        current_yearday, current_time = date_split(current_index_datetime)

        # recall the TIME in consistent across the 65536 area:
        matrix_time_inserted[
            :, :, index_time_increment, 17
        ] = current_yearday  # 17 = 18th position in zero based index.
        matrix_time_inserted[
            :, :, index_time_increment, 18
        ] = current_time  # 18 = 19th position in zero based index.

    logger.info(matrix_time_inserted.shape)

    # Keeping the LAST dimension after the rearrangement:
    # matrix_3d should now be 256 x 256, 19
    matrix_4d = matrix_time_inserted.reshape(
        (size_temporal_dimension, size_x_dimension, size_y_dimension, 19)
    )
    logger.info(matrix_4d.shape)

    return matrix_4d


class DataSequence(Sequence):
    """
    A customary sequence class to help obtain the input necessary for training.
    This one does factor in the temporal dimension, IGNORE spatial dimension.
    This one is customized to read the weather NPY data from the weather network challenge.
    """

    def __init__(
        self, date_txt_path: str, batch_size: int = 5, mode=ProcessingMode.Train
    ):
        """
        :param date_txt_path: the path to the folder which contain all the 3D inputs and 2D labels paired per timepoint
        :param batch_size: how many such 3D and 2D pairs within each batch.
        :param mode:
        """

        # read the csv file with pandas
        # self.df = pd.read_csv(path_data_root)
        path_folder = os.path.dirname(date_txt_path)

        self.x_files = recursive_list_re(date_txt_path, "input")
        self.y_files = recursive_list_re(date_txt_path, "label")

        # Extract time points.
        self.timestamps = []  # LIST of date times gleened from the file names.
        for file in self.x_files:
            self.timestamps.append(decipher_isodatestr(file))

        assert len(self.x_files) == len(self.y_files) == len(self.timestamps)

        # This keeps track of the order of drawing and is randomized each time.
        self.draw_indices = list(range(len(self.x_files)))

        # seed the random generator for reproducible pseudorandomness
        random.seed("Nice")
        random.shuffle(
            self.draw_indices
        )  # shuffle the list which is made from 0 to length of the x files.

        # FILE/Temporal loop

        # How many files to concatenate, there are 53! and we best not to train on one file as that is limited to 3 months period.

        """# Few files, load them all.
        if len(self.file_lines) < 5:

        # otherwise, load way more.
        else:
            num_files = 3
            for x in range(num_files):
                # Generate
                chosen_file_index = randint(0, len(self.file_lines)-1)
                chosen_file = self.file_lines[chosen_file_index]
                list_random_files.append(chosen_file)"""

        # batch size
        self.batch_size = batch_size

        # shuffle when in train mode
        self.mode = mode

        # Ground Truth Temperature from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_ground_truth_2d = None

        # List of variables from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_input_3ds = None  # LIST of 3D numpy array.

        self.nparray_fixed = (
            None
        )  # this stores the matrix of data that were not varied across datasets and are strctly used as a constant input.

        new_path = Path(date_txt_path) / f"{unique_name()}.csv"

        self.generate_csv(new_path)

    def generate_csv(self, csv_path):
        """
        Output the randomized order into CSV for tracking purposes.
        :param folder_path:
        :param csv_path:
        :return:
        """

        import csv

        # Open files for writing.
        with open(csv_path, "w", newline="") as csv_file:

            # Open file
            csv_writer = csv.DictWriter(
                csv_file, fieldnames=["Order", "Index", "DataNPY", "LabelNPY"]
            )
            csv_writer.writeheader()
            order = 0
            for index in self.draw_indices:
                # Write to CSV
                csv_writer.writerow(
                    {
                        "Order": order,
                        "Index": index,
                        "DataNPY": self.x_files[index],
                        "LabelNPY": self.y_files[index],
                    }
                )
                order += 1
        return csv_path

    def __len__(self):

        # compute number of batches to yield
        # Note that since 5342 / 16 = 333.8, it means, that there are only 333 FULL batches and 4 or so cases are left over.
        # Hence the length of the sequence should be floored instead of ceiling unless somehow I find a way to
        return int(
            # Floor resturn only a double.
            math.floor(len(self.x_files) / float(self.batch_size))
        )

    def on_epoch_end(self):
        # fixme: is this really necessary to shuffle at epoch end? Also, since we are using the same seed... may be problematic?
        # Shuffles indexes after each epoch if in training mode
        random.shuffle(self.draw_indices)

    def get_batch_labels(self, index_batch):
        """
        Fetch a batch of labels
        :param index_batch:
        :return:
        """

        nparray_ground_truth_4d = None

        # First batch = 0 to Batch_size
        # So array [0, X] to [Batch_size-1, X]
        # this can be written as [0:Batch_size, X]
        index_first_element = index_batch * self.batch_size
        index_final_excluded_element = (index_batch + 1) * self.batch_size
        # note that from first element to first element to exclude, there are BATCH size number of items. As the second number is INDICATIVE of where to stop the indexing.

        # Loop through each file.
        for index in range(index_first_element, index_final_excluded_element):

            # the numerical index of the SINGLE file to be processed among the 6K npy files to be processed.
            index_of_single_file = self.draw_indices[index]

            # y should have 1 x 256 x 256
            label_matrix_3d = np.load(self.y_files[index_of_single_file])

            label_matrix_3d_reordered = label_matrix_3d.transpose(1, 2, 0)

            # Concatenate this to the object level list
            if nparray_ground_truth_4d is None:
                nparray_ground_truth_4d = label_matrix_3d_reordered[np.newaxis, :, :, :]
            else:
                nparray_ground_truth_4d = np.vstack(
                    (
                        nparray_ground_truth_4d,
                        label_matrix_3d_reordered[np.newaxis, :, :, :],
                    )
                )

        return nparray_ground_truth_4d

    def get_batch_dates(self, index_batch):
        """
        This retrieve the dates into a matrix to be returned.

        :param index_batch:
        :return:
        """
        # Starting point.
        index_first_element = index_batch * self.batch_size

        # Ending point.
        index_final_excluded_element = (1 + index_batch) * self.batch_size

        date_array_2d = None

        # Loop through each file.
        for index in range(index_first_element, index_final_excluded_element):

            # the numerical index of the SINGLE file to be processed among the 6K npy files to be processed.
            datestring = self.timestamps[index]
            day_ofyear, hour_ofday = date_split(tstr2iso(datestring))

            current_date = np.array([day_ofyear, hour_ofday])

            # Check before concatenate.
            if date_array_2d is None:
                date_array_2d = current_date
            else:
                date_array_2d = np.vstack((date_array_2d, current_date))

        return date_array_2d

    def get_batch_features(self, index_batch):
        """
        This retrieve the high dimensional data of the number of timepoints as a numpy array ONLY when needed.

        :param index_batch:
        :return:
        """
        # Starting point.
        index_first_element = index_batch * self.batch_size

        # Ending point.
        index_final_excluded_element = (1 + index_batch) * self.batch_size

        nparray_input_4d = None

        # Loop through each file.
        for index in range(index_first_element, index_final_excluded_element):

            # Help with debug range bound error
            if index not in range(0, 5343):
                raise ValueError

            # the numerical index of the SINGLE file to be processed among the 6K npy files to be processed.
            index_of_single_file = self.draw_indices[index]

            # x should be 15 x 256 x 256
            matrix3d_raw = np.load(self.x_files[index_of_single_file])

            # Strip the 0 to 3 layer in the 3D loaded.
            # removing var0, var1, var2, var3 because they are PERMANENTLY constant.
            matrix3d_filtered = matrix3d_raw[4:15, :, :]

            # x should be transposed: to 256 x 256 x 15
            matrix3d_raw_reordered = matrix3d_filtered.transpose([1, 2, 0])

            # Check before concatenate.
            if nparray_input_4d is None:
                nparray_input_4d = matrix3d_raw_reordered[np.newaxis, :, :, :]
            else:
                nparray_input_4d = np.vstack(
                    (nparray_input_4d, matrix3d_raw_reordered[np.newaxis, :, :, :])
                )

        return nparray_input_4d

    def __getitem__(self, batch_index):
        """
        Implement the necessary function which get next items are required by generator classes to load data.
        :param batch_index:
        :return:
        """
        batch_x1 = self.get_batch_features(batch_index)
        batch_x2 = self.get_batch_dates(batch_index)
        batch_y = self.get_batch_labels(batch_index)
        return [batch_x1, batch_x2], batch_y


if __name__ == "__main__":
    # Debug path
    path_validate_spec = r"E:\WeatherData\data\stamped"
    data = DataSequence(path_validate_spec, 2, ProcessingMode.Train)
    pass
