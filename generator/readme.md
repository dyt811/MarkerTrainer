## /generator

Most of the time, data cannot fit into GPU memory and has to be parcellated into smaller chunks using Keras generator format. Depending on the complexity of the data/model, different generators will be required. 
