from keras.utils import Sequence
from keras_preprocessing.image import load_img
import random
from pathlib import Path
from PythonUtils.PUFolder import recursive_list
from PythonUtils.PUFile import unique_name
import numpy as np
import logging
from generator.current_mode import ProcessingMode
import csv

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger()

# This is the Data Sequence class for loading image input and output for DeepLabV3+.


class DataSequence(Sequence):
    """
    A customary sequence class to help obtain the input necessary for training, this one is for DeepLabV3+ specifically,
    tailoring to its requirement of have ground truth in 2D gray scale dimensions
    """

    def __init__(
        self, path_data_root: Path, batch_size: int = 25, mode=ProcessingMode.Train
    ):
        """
        :param path_data_root: the path to the folder which contain all the 3D inputs and 2D labels paired per timepoint
        :param batch_size: how many such 3D and 2D pairs within each batch.
        :param mode: default processing mode is to train.
        """

        # read the csv file with pandas
        # self.df = pd.read_csv(path_data_root)
        path_train = path_data_root.joinpath(Path("train"))
        path_label = path_data_root.joinpath(Path("label"))

        self.x_files = recursive_list(path_train)
        self.y_files = recursive_list(path_label)

        # Sanity check to ensure x and y files have the same dimension.
        assert len(self.x_files) == len(self.y_files)

        # This keeps track of the order of drawing and is randomized each time.
        self.draw_indices = list(range(len(self.x_files)))

        # seed the random generator for reproducible pseudorandomness
        random.seed("Nice")
        random.shuffle(
            self.draw_indices
        )  # shuffle the list which is made from 0 to length of the x files.

        # FILE/Temporal loop

        # batch size
        self.batch_size = batch_size

        # shuffle when in train mode
        self.mode = mode

        # Ground Truth Temperature from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_ground_truth_2d = None

        # List of variables from X files across N timepoints, across all possible X Y coordinates.
        self.nparray_input_3ds = None  # LIST of 3D numpy array.

        new_path = Path(path_data_root) / f"{unique_name()}.csv"

        self.generate_csv(new_path)

    def generate_csv(self, csv_path):
        """
        Output the randomized order into CSV for tracking purposes.
        :param folder_path:
        :param csv_path:
        :return:
        """

        # Open files for writing.
        with open(csv_path, "w", newline="") as csv_file:

            # Open file
            csv_writer = csv.DictWriter(
                csv_file, fieldnames=["Order", "Index", "DataNPY", "LabelNPY"]
            )
            csv_writer.writeheader()
            order = 0
            for index in self.draw_indices:
                # Write to CSV
                csv_writer.writerow(
                    {
                        "Order": order,
                        "Index": index,
                        "DataNPY": self.x_files[index],
                        "LabelNPY": self.y_files[index],
                    }
                )
                order += 1
        return csv_path

    def __len__(self):
        # compute number of batches to yield
        return len(self.x_files) // self.batch_size

    def on_epoch_end(self):
        # fixme: is this really necessary to shuffle at epoch end? Also, since we are using the same seed... may be problematic?
        # Shuffles indexes after each epoch if in training mode
        random.shuffle(self.draw_indices)

    def get_batch_labels(self, index_batch):
        """
        Fetch a batch of labels, in this case, they would be GRAY images of known dimensions with limited color channels
        :param index_batch:
        :return:
        """
        nparray_ground_truth_4d = None

        # First batch = 0 to Batch_size
        # So array [0, X] to [Batch_size-1, X]
        # this can be written as [0:Batch_size, X]

        # note that from first element to first element to exclude, there are BATCH size number of items. As the second number is INDICATIVE of where to stop the indexing.

        # Starting point.
        index_first_element = index_batch * self.batch_size

        # Ending point.
        index_final_excluded_element = (index_batch + 1) * self.batch_size

        # Loop through each file.
        for index in range(index_first_element, index_final_excluded_element):

            # the numerical index of the SINGLE file to be processed among the image files to be processed.
            index_of_single_file = self.draw_indices[index]

            # x should be 256 x 1600 x 4 Defects Channels
            label_matrix_3d = np.load(self.y_files[index_of_single_file])

            # Concatenate this to the object level list
            if nparray_ground_truth_4d is None:
                nparray_ground_truth_4d = label_matrix_3d[np.newaxis, :, :, :]
            else:
                nparray_ground_truth_4d = np.vstack(
                    (nparray_ground_truth_4d, label_matrix_3d[np.newaxis, :, :, :])
                )
        return nparray_ground_truth_4d

    def get_batch_features(self, index_batch):
        """
        Fetch a batch of image input, in this case, they would be RGB images of known dimensions.

        :param index_batch:
        :return:
        """
        nparray_input_4d = None

        # First batch = 0 to Batch_size
        # So array [0, X] to [Batch_size-1, X]
        # this can be written as [0:Batch_size, X]

        # note that from first element to first element to exclude, there are BATCH size number of items. As the second number is INDICATIVE of where to stop the indexing.

        # Starting point.
        index_first_element = index_batch * self.batch_size

        # Ending point.
        index_final_excluded_element = (1 + index_batch) * self.batch_size

        # Loop through each file.
        for index in range(index_first_element, index_final_excluded_element):

            # the numerical index of the SINGLE file to be processed among the 6K npy files to be processed.
            index_of_single_file = self.draw_indices[index]

            # x should be width x height x RGB
            input_matrix_3d = np.array(load_img(self.x_files[index_of_single_file]))

            # Check before concatenate.
            if nparray_input_4d is None:
                nparray_input_4d = input_matrix_3d[np.newaxis, :, :, :]
            else:
                nparray_input_4d = np.vstack(
                    (nparray_input_4d, input_matrix_3d[np.newaxis, :, :, :])
                )
        return nparray_input_4d

    def __getitem__(self, batch_index):
        """
        Implement the necessary function which get next items are required by generator classes to load data.
        :param batch_index:
        :return:
        """
        batch_x = self.get_batch_features(batch_index)
        batch_y = self.get_batch_labels(batch_index)
        return batch_x, batch_y


if __name__ == "__main__":
    # Debug path
    path_validate_spec = r"E:\WeatherData\data\stamped"
    data = DataSequence(path_validate_spec, 2, ProcessingMode.Train)
    pass
